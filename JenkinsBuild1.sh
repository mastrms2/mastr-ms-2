#!/bin/sh
#
# Script to control Mastrms in dev and test
#

# break on error
set -e -x

ACTION=$1

PORT='8000'

PROJECT_NAME='mastrms'
VIRTUALENV="${WORKSPACE}/env"
CUSTOMTEST=${WORKSPACE}/mastrms/mastrms/custom_tests

activate_virtualenv() {
    . ${VIRTUALENV}/bin/activate
}

deactivate_virtualenv() {
    deactivate
}

make_virtualenv() {
    # check requirements
    which virtualenv > /dev/null
    if [ ! -e ${VIRTUALENV} ]; then
        virtualenv --no-site-packages ${VIRTUALENV}
    fi

    activate_virtualenv
    
    #Build the whole software
    pip install 'flake8>=2.0,<2.1'
    pip install 'closure-linter==2.3.13'
    pip install -r ${WORKSPACE}/mastrms/all-requirements.txt
    pip install ${WORKSPACE}/mdatasync_client/
    pip install -e ${WORKSPACE}/mastrms
    pip install --quiet nosexcover
    pip install --quiet pylint
}

# Clean old virutalenv 
if [ -d $VIRTUALENV ]; then
   rm -rf $VIRTUALENV
fi

# Make the new virtualenv and build the software
make_virtualenv

# Run the unit test for compression  and generate test/coverage/code quality reports 
cd $CUSTOMTEST
nosetests --with-xcoverage --with-xunit --cover-erase --cover-package=${WORKSPACE}/mastrms/mastrms
cd $WORKSPACE
pylint -f parseable $WORKSPACE/mastrms/mastrms | tee pylint.out


# Uncomment the following two lines if running web service is needed
#chmod 755 ${WORKSPACE}/docker-entrypoint.sh 
#${WORKSPACE}/docker-entrypoint.sh runserver




# Dactivate the virutalenv
#deactivate_virtualenv


