/**
 * This file is part of Madas.
 *
 * Madas is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Madas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Madas.  If not, see <http://www.gnu.org/licenses/>.
 */

var cacheParams = [];
var isLoaded = false;

MA.ReportInit = function() {
  //Will fill with something 
  cacheParams = [];
  isLoaded = false;
}

var downloadToCSVHandler = function (el, ev) {
    if (cacheParams != []) {
        if (isLoaded) {
          window.location = MA.BaseUrl + 'report/processDownload/?' + paramGenerator(cacheParams);
        }
    }
};

var paramGenerator = function(params) {
    var paramStr = "";
    for (i = 0; i < params.length; i++) {
        paramStr = paramStr + params[i]["id"]+"="+params[i]["value"];
        if (i < params.length-1) {
            paramStr = paramStr + "&";
        }
    }
    return paramStr;
}
;

MA.ReportCmp = {
    id: 'report-container-panel',
    layout: 'absolute',
    autoScroll: true,
    deferredRender: false,
    forceLayout: true,
    defaults: {
        deferredRender: false,
        forceLayout: true
    },
    items: [{ 
            xtype: 'form',
            labelWidth: 80, // label settings here cascade unless overridden
            id: 'report-panel',
            frame: true,
            title: 'Report',
            bodyStyle: 'padding:10px 50px 0',
            width: 1000,
            x: 50,
            y: 10,
            defaults: {width: 400},
            defaultType: 'textfield',
            trackResetOnLoad: true,
            waitMsgTarget: true,
            items: [{ xtype: 'fieldset',
                      title: 'Date',
                      id: 'dateFieldset',
                      layout: 'hbox',
                      style: 'padding: 5px',
                      autoHeight: true,
                      labelWidth: 50,
                      width: '100%',
                      items: [
                              { xtype: 'datefield', width: '40%', fieldLabel: 'From', id: 'date_start', format : 'd-m-Y' },
                              { xtype: 'tbspacer', width: '20%' },
                              { xtype: 'datefield', width: '40%', fieldLabel: 'To', id: 'date_end', format : 'd-m-Y' }
                      ]
            },{ xtype: 'fieldset',
                title: 'Client Node',
                id: 'clientNodeFieldset',
                style: 'padding: 5px',
                autoHeight: true,
                labelWidth: 50,
                width: '100%',
                items: [
                        new Ext.form.ComboBox({
                                       fieldLabel: 'Node',
                                       id: 'site_name',
                                       editable: true,
                                       displayField: 'site_name',
                                       valueField: 'site_name',
                                       lazyRender: true,
                                       typeAhead: false,
                                       mode: 'local',
                                       queryParam: false,
                                       triggerAction: 'all',
                                       width: '100%',
                                       listWidth: 230,
                                       store: machineStore
                                       }),
                        new Ext.form.ComboBox({
                                       fieldLabel: 'Instrument',
                                       id: 'station_name',
                                       editable: true,
                                       displayField: 'station_name',
                                       valueField: 'station_name',
                                       lazyRender: true,
                                       typeAhead: false,
                                       mode: 'local',
                                       queryParam: false,
                                       triggerAction: 'all',
                                       width: '100%',
                                       listWidth: 230,
                                       store: machineStore
                                       })
                ]
            },{ xtype: 'fieldset',
                title: 'Client',
                id: 'clientFieldset',
                style: 'padding: 5px',
                autoHeight: true,
                labelWidth: 50,
                width: '100%',
                items: [
                        new Ext.form.ComboBox({
                                       fieldLabel: 'Client Name',
                                       id: 'client_name',
                                       editable: true,
                                       displayField: 'name',
                                       valueField: 'email',
                                       lazyRender: true,
                                       typeAhead: false,
                                       mode: 'local',
                                       queryParam: false,
                                       triggerAction: 'all',
                                       width: '100%',
                                       listWidth: 230,
                                       store: userListStore
                                    })
                ]
            },{ xtype: 'fieldset',
                title: 'Project Manager',
                id: 'projectManagerFieldset',
                style: 'padding: 5px',
                autoHeight: true,
                labelWidth: 50,
                width: '100%',
                items: [
                        new Ext.form.ComboBox({
                                       fieldLabel: 'Project Manager Name',
                                       id: 'projectmanager_name',
                                       editable: true,
                                       displayField: 'name',
                                       valueField: 'email',
                                       lazyRender: true,
                                       typeAhead: false,
                                       mode: 'local',
                                       queryParam: false,
                                       triggerAction: 'all',
                                       width: '100%',
                                       listWidth: 230,
                                       store: userListStore
                                    })
                ]
            }],
            buttons : [ {
              text : 'Submit',
              handler: function() {
                 var form = Ext.getCmp('report-panel').getForm()
                 var grid = Ext.getCmp('report-result-grid')
                 grid.store.removeAll()
                 form.submit({
                    url : MA.BaseUrl + 'report/process/',
                    method : 'POST',
                    successProperty: 'success',
                    success: function(form, action) {
                        if (action.result.success === true) {
                            //display a success alert that auto-closes in 5 seconds

                            if (action.result.data.length > -1) {
                                if (grid != undefined) {
                                    grid.store.loadData(action.result.data);
                                    isLoaded = true;
                                }
                            }
                            var dateStartField = form.findField('date_start');
                            var dateEndField = form.findField('date_end');
                            var siteNameField = form.findField('site_name');
                            var stationNameField = form.findField('station_name');
                            var clientNameField = form.findField('client_name');
                            var pmNameField = form.findField('projectmanager_name');
                            cacheParams = [
                               {'id':dateStartField.getInputId(),'value':dateStartField.getValue()},
                               {'id':dateEndField.getInputId(),'value':dateEndField.getValue()},
                               {'id':siteNameField.getInputId(),'value':siteNameField.getValue()},
                               {'id':stationNameField.getInputId(),'value':stationNameField.getValue()},
                               {'id':clientNameField.getInputId(),'value':clientNameField.getValue()},
                               {'id':pmNameField.getInputId(),'value':pmNameField.getValue()}
                            ];
                        }
                    },
                    failure: function(form, action) {
                    //do nothing special. this gets called on validation failures and server errors
                    }
                });
              }
        }]
    }, {
        xtype: 'panel',
        title: 'Report Runs Result',
        x: 50,
        y: 450,
        id: 'report-runs-panel',
        tbar: new Ext.Toolbar({
            items: [
                { id: 'downloadCSVBtn', text: 'Download to CSV', handler: downloadToCSVHandler }
            ]
        }),
        items: [{
            xtype: 'grid',
            id: 'report-result-grid',
            border: false,
            autoScroll: true,
            stripeRows: true,
            width : '100%', 
            selModel: {
                mode : 'SINGLE'
            },
            store: new Ext.data.Store({
                id: 'reportRId',
                fields: [
                        { name: 'runs_id', sortType: Ext.data.SortTypes.asInt },
                        { name: 'title', sortType: Ext.data.SortTypes.asText },
                        { name: 'site_name', sortType: Ext.data.SortTypes.asText },
                        { name: 'station_name', sortType: Ext.data.SortTypes.asText },
                        { name: 'experiment_title', sortType: Ext.data.SortTypes.asText },
                        { name: 'project_id', sortType: Ext.data.SortTypes.asText },
                        { name: 'project_title', sortType: Ext.data.SortTypes.asText },
                        { name: 'client_email', sortType: Ext.data.SortTypes.asText },
                        { name: 'pm_email', sortType: Ext.data.SortTypes.asText },
                        { name: 'run_createdon', sortType: Ext.data.SortTypes.asText }
                ],
                //groupField: 'runs_id',
                autoLoad: true
            }),
            features: [{
                id: 'group',
                ftype: 'groupingsummary',
                groupHeaderTpl: 'Run ID : {name}',
                hideGroupedHeader: true,
                enableGroupingMenu: false
            }],
            listeners: {
                rowdblclick: function(grid, record, tr, rowIndex, evt) {
                    //console.log(record);
                    MA.LoadProject(record.data.project_id);
                }
            },
            columns: [{
                    header: 'Run ID',
                    sortable: true,
                    width: '4%',
                    dataIndex: 'runs_id'
                },{
                    header: 'Title',
                    sortable: true,
                    width: '12%',
                    dataIndex: 'title'
                },{
                    header: 'Node',
                    width: '12%',
                    dataIndex: 'site_name'
                },{
                    header: 'Instrument',
                    width: '12%',
                    dataIndex: 'station_name'
                },{
                    header: 'Experiment Title',
                    width: '12%',
                    dataIndex: 'experiment_title'
                },{
                    header: 'Project Title',
                    width: '12%',
                    dataIndex: 'project_title'
                },{
                    header: 'Client Email',
                    width: '12%',
                    dataIndex: 'client_email'
                },{
                    header: 'PM Email',
                    width: '12%',
                    dataIndex: 'pm_email'
                },{
                    header: 'Created On',
                    width: '12%',
                    dataIndex: 'run_createdon'
                }
            ]
        }]

    }]
};
