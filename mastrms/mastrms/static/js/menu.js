MA.MenuRender = function(email) {

    var userText = 'User: ' + email;

    var tb = new Ext.Toolbar(

        {
            id: 'toolbara',
            items: [
                { xtype: 'button', text: 'Login', id: 'login', handler: MA.MenuHandler},
                { xtype: 'button', text: 'Dashboard', id: 'dashboard', handler: MA.MenuHandler},
                { xtype: 'button', text: 'Admin', id: 'admin', menu: {
                    items: [
                        {text: 'Admin Requests', id: 'admin_adminrequests', handler: MA.MenuHandler},
                        {text: 'Active User Search', id: 'admin_usersearch', handler: MA.MenuHandler},
                        {text: 'Rejected User Search', id: 'admin_rejectedUsersearch', handler: MA.MenuHandler},
                        {text: 'Deleted User Search', id: 'admin_deletedUsersearch', handler: MA.MenuHandler},
                        new Ext.menu.Separator(),
                        {text: 'Node Management', id: 'admin_nodelist', handler: MA.MenuHandler},
                        {text: 'Organisation Management', id: 'admin_orglist', handler: MA.MenuHandler}
                    ]
                    }
                },
                { xtype: 'button', text: 'Quotes', id: 'quote', menu: {
                    items: [
                        {text: 'Make an Inquiry', id: 'quote_request', handler: MA.MenuHandler},
                        {text: 'View Quote Requests', id: 'quote_list', handler: MA.MenuHandler},
                        //{text: 'My Formal Quotes', id: 'quote_listFormal', handler: MA.MenuHandler},
                        {text: 'Overview List', id: 'quote_listAll', handler: MA.MenuHandler}
                    ]
                    }
                },
                { xtype: 'button', text: 'Repository', id: 'repo', menu: {
                    items: [
                        {text: 'Projects', id: 'project_list', handler: MA.MenuHandler},
                        {text: 'Clients', id: 'client_list', handler: MA.MenuHandler},
                        {text: 'Runs', id: 'run_list', handler: MA.MenuHandler},
                        {text: 'Rule Generators', id: 'rulegenerator_list', handler: MA.MenuHandler},
                        new Ext.menu.Separator(),
                        {text: 'Admin', id: 'repo_admin', handler: MA.MenuHandler}
                    ]
                    }
                },
                { xtype: 'button', text: 'Report', id: 'report', handler: MA.MenuHandler},
                { xtype: 'button', text: 'Help', id: 'help', menu: {
                    items: [
                            {text: 'Screencasts', id: 'help_screencasts', menu: {
                            items: [
                                    {text: 'Requesting a Quote', id: 'help_screencasts-quoterequest', handler: MA.MenuHandler}
                                    ]
                            }
                            },
                            {text: 'Admin screencasts', id: 'helpadmin_screencasts', menu: {
                            items: [
                                    {text: 'Accepting/rejecting users', id: 'helpadmin_screencasts-authrequest', handler: MA.MenuHandler},
                                    {text: 'Forwarding a Quote Request', id: 'helpadmin_screencasts-forwardquoterequest', handler: MA.MenuHandler},
                                    {text: 'Sending a Formal Quote', id: 'helpadmin_screencasts-forwardformal', handler: MA.MenuHandler},
                                    {text: 'Replacing a Formal Quote', id: 'helpadmin_screencasts-replaceformal', handler: MA.MenuHandler}
                                    ]
                            }
                            },
                            {text: 'Contact Us', id: 'help_contactus', handler: MA.MenuHandler }
                            ]
                    }
                },

                { xtype: 'tbfill'},
                { xtype: 'button', text: userText, id: 'userMenu', menu: {
                    items: [
                        {text: 'Logout', id: 'login_processLogout', handler: MA.LogoutHandler},
                        {text: 'My Account', id: 'user_myaccount', handler: MA.MenuHandler}
                    ]
                    }
                }
            ]
        }

    );
    tb.render('toolbar');

};

MA.MenuEnsure = function() {
    if (MA.CurrentUser.IsLoggedIn) {
        MA.MenuShow();
    }
    else {
        MA.MenuHide();
    }
};

MA.MenuShow = function() {
    var isPrivileged = (MA.CurrentUser.IsAdmin || MA.CurrentUser.IsMastrAdmin || MA.CurrentUser.IsNodeRep || MA.CurrentUser.IsProjectLeader);
    Ext.BLANK_IMAGE_URL = MA.BaseUrl + 'static/images/s.gif';

    //disable certain menu items if the user is not an admin
    if (isPrivileged) {
        Ext.get('admin').show();
    } else {
        Ext.get('admin').hide();

    }
    Ext.getCmp('admin_nodelist').setDisabled(!MA.CurrentUser.IsAdmin);
    Ext.getCmp('admin_orglist').setDisabled(!MA.CurrentUser.IsAdmin);
    Ext.getCmp('helpadmin_screencasts').setDisabled(!MA.CurrentUser.IsAdmin);

    Ext.getCmp('admin_adminrequests').setDisabled(!(MA.CurrentUser.IsAdmin || MA.CurrentUser.IsNodeRep));
    Ext.getCmp('admin_usersearch').setDisabled(!isPrivileged);
    Ext.getCmp('admin_rejectedUsersearch').setDisabled(!(MA.CurrentUser.IsAdmin || MA.CurrentUser.IsNodeRep));
    Ext.getCmp('admin_deletedUsersearch').setDisabled(!(MA.CurrentUser.IsAdmin || MA.CurrentUser.IsNodeRep));

    Ext.getCmp('repo_admin').setDisabled(!(MA.CurrentUser.IsAdmin || MA.CurrentUser.IsMastrAdmin));
    Ext.getCmp('client_list').setDisabled(!(MA.CurrentUser.IsAdmin || MA.CurrentUser.IsMastrAdmin || MA.CurrentUser.IsProjectLeader));

    Ext.get('login').hide();
    Ext.get('dashboard').show();
    Ext.get('userMenu').show();
    Ext.get('report').show();
    Ext.getCmp('quote_list').show();
    Ext.getCmp('quote_listAll').setDisabled(!(MA.CurrentUser.IsAdmin || MA.CurrentUser.IsNodeRep));
    //Ext.getCmp('quote_listFormal').show();

    if (MA.CurrentUser.IsAdmin || MA.CurrentUser.IsMastrAdmin || MA.CurrentUser.IsProjectLeader || MA.CurrentUser.IsMastrStaff) {
        Ext.get('repo').show();
    } else {
        Ext.get('repo').hide();
    }

};

MA.MenuHandler = function(item, params) {
    //we authorize every access to check for session timeout and authorization to specific pages
    //if (item.id.substr(0,4) == "help") {
        MA.ChangeMainContent(item.id, params);
    //} else {
    //    MA.Authorize(item.id);
    //}
};

MA.LogoutHandler = function() {
    window.location = MA.BaseUrl + "login/processLogout";
};

MA.MenuHide = function() {

    Ext.get('login').show();
    Ext.get('dashboard').hide();
    Ext.get('admin').hide();
    Ext.get('userMenu').hide();
    Ext.getCmp('quote_list').hide();
    Ext.getCmp('quote_listAll').hide();
    //Ext.getCmp('quote_listFormal').hide();
    Ext.get('repo').hide();
    Ext.get('report').hide();
    Ext.getCmp('helpadmin_screencasts').disable();
};
