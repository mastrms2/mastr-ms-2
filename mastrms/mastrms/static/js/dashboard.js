/**
 * This file is part of Madas.
 *
 * Madas is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Madas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Madas.  If not, see <http://www.gnu.org/licenses/>.
 */

MA.Dashboard.CreatePendingUserRequests = function() {
    var dataurl = MA.BaseUrl + 'admin/adminrequests';

    function editUser(email) {
        MA.ChangeMainContent('admin:useredit', [email]);
    }

    var editHandler = function(el, ev) {
        var grid = panel.items.items[0];
        var selectionModel = grid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            editUser(selectionModel.getSelection()[0].data.email);
        }
    };

    var toolbar = new Ext.Toolbar({
        items: [
            { id: 'userEditBtn', text: 'Edit', handler: editHandler }
        ]
    });

    var panel = new Ext.Panel({
        title: 'Pending User Requests',
        style: 'padding:20px 20px 0px 20px',
        id: 'dashboard-pending-user-requests',
        tbar: toolbar,
        init: function() {
            this.items.items[0].store.load();
        },
        items: [{
            xtype: 'grid',
            border: false,
            autoScroll: true,
            stripeRows: true,
            width: '100%',
            viewConfig: {
                loadMask: false
            },
            autoExpandColumn: 'email',
            store: new Ext.data.Store({
                id: 'bSId',
                storeId: 'bSId',
                fields: [
                    { name: 'isClient', sortType: Ext.data.SortTypes.asText },
                    { name: 'email', sortType: Ext.data.SortTypes.asText },
                    { name: 'firstname', sortType: Ext.data.SortTypes.asText },
                    { name: 'lastname', sortType: Ext.data.SortTypes.asText },
                    { name: 'telephoneNumber', sortType: Ext.data.SortTypes.asText },
                    { name: 'physicalDeliveryOfficeName', sortType: Ext.data.SortTypes.asText },
                    { name: 'title', sortType: Ext.data.SortTypes.asText }
                ],
                sortInfo: {field: 'lastname', direction: 'ASC'},
                proxy: new Ext.data.HttpProxy ({
                    url: dataurl,
                    reader: {
                        type: 'json',
                        rootProperty: 'response.value.items',
                        versionProperty: 'response.value.version',
                        totalProperty: 'response.value.total_count'
                    }
                })
            }),
            selModel: {
                mode : 'SINGLE'
            },
            listeners: {
                rowdblclick: function(grid, record, tr, rowIndex, evt) {
                    editUser(record.get('email'));
                }
            },
            columns: [{
                    header: 'First Name',
                    sortable: true,
                    width: '30%',
                    dataIndex: 'firstname'
                },{
                    header: 'Last Name',
                    sortable: true,
                    width: '30%',
                    dataIndex: 'lastname'
                },{
                    width: '40%',
                    header: 'E-mail',
                    dataIndex: 'email'
                }
            ]
        }]
    });
    
    return panel;
   };

MA.Dashboard.CreateQuotesRequiringAttention = function() {
    var dataurl = MA.BaseUrl + 'quote/listNeedsAttention';

    var editQuote = function(quote_id) {
        MA.ChangeMainContent('quote:edit', [quote_id]);
    };
    var quoteEditHandler = function(el, ev) {
        var grid = panel.items.items[0];
        var selectionModel = grid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            editQuote(selectionModel.getSelection()[0].data.id);
        }
    };

    var toolbar = new Ext.Toolbar({
        items: [
            { id: 'quoteEditBtn', text: 'Edit', handler: quoteEditHandler }
        ]
    });

    var panel = new Ext.Panel({
        title: 'Quotes Requiring Attention',
        style: 'padding:20px 20px 0px 20px',
        id: 'dashboard-quotes-requiring-attention',
        tbar: toolbar,
        init: function() {
            this.items.items[0].store.load();
        },
        items: [{
            xtype: 'grid',
            border: false,
            autoScroll: true,
            stripeRows: true,
            //height: 150,
            width: '100%',
            store: new Ext.data.Store({
                id: 'bSId',
                fields: [
                        { name: 'isClient', sortType: Ext.data.SortTypes.asText },
                        { name: 'email', sortType: Ext.data.SortTypes.asText },
                        { name: 'firstname', sortType: Ext.data.SortTypes.asText },
                        { name: 'lastname', sortType: Ext.data.SortTypes.asText },
                        { name: 'telephoneNumber', sortType: Ext.data.SortTypes.asText },
                        { name: 'physicalDeliveryOfficeName', sortType: Ext.data.SortTypes.asText },
                        { name: 'title', sortType: Ext.data.SortTypes.asText }
                ],
                sortInfo: {field: 'requesttime', direction: 'DESC'},
                proxy: new Ext.data.HttpProxy ({
                    url: dataurl,
                    reader: {
                        type: 'json',
                        rootProperty: 'response.value.items',
                        versionProperty: 'response.value.version',
                        totalProperty: 'response.value.total_count'
                    }
                })
            }),
            selMode : {
                mode : 'SINGLE'
            },
            listeners: {
                rowdblclick: function(grid, record, tr, rowIndex, evt) {
                    editQuote(record.get('id'));
                }
            },
            columns: [
                       {
                            header: 'ID',
                            sortable: true,
                            width: '10%',
                            dataIndex: 'id'
                        },
                        {
                            header: 'Unread',
                            sortable: true,
                            width: '10%',
                            renderer: MA.Utils.GridCheckboxRenderer,
                            dataIndex: 'unread'
                        },
                        {
                            header: 'E-mail',
                            width: '20%',
                            dataIndex: 'email'
                        },
                        {
                            header: 'First Name',
                            sortable: true,
                            width: '15%',
                            dataIndex: 'firstname'
                        },
                        {
                            header: 'Last Name',
                            sortable: true,
                            width: '15%',
                            dataIndex: 'lastname'
                        },
                        {
                            header: 'Node',
                            sortable: true,
                            width: '15%',
                            dataIndex: 'tonode'
                        },
                        {
                            header: 'Date Received',
                            sortable: true,
                            width: '15%',
                            dataIndex: 'requesttime'
                        }
                     ]
              }
       ]
    });
    return panel;
   };

MA.Dashboard.CreateRecentProjects = function() {
    var dataurl = MA.BaseUrl + 'ws/recent_projects';

    Ext.define('MadasRecentProjectsModel', {
        extend: 'Ext.data.Model',
        fields: [
            { name: 'id', sortType: Ext.data.SortTypes.asInt },
            { name: 'title', sortType: Ext.data.SortTypes.asText },
            { name: 'client', sortType: Ext.data.SortTypes.asText }
        ]
    });

    var editProject = function(project_id) {
        MA.LoadProject(project_id);
    };
    var projectEditHandler = function(el, ev) {
        var grid = panel.items.items[0];
        var selectionModel = grid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            editProject(selectionModel.getSelected().data.id);
        }
    };

    var toolbar = new Ext.Toolbar({
        items: [
            { id: 'projectEditBtn', text: 'Edit', handler: projectEditHandler }
        ]
    });

    var panel = new Ext.Panel({
        title: 'Recent Projects',
        style: 'padding:20px 20px 0px 20px',
        id: 'dashboard-recent-projects',
        tbar: toolbar,
        init: function() {
            this.items.items[0].store.load();
        },
        items: [{
            xtype: 'grid',
            border: false,
            autoScroll: true,
            stripeRows: true,
            height: 150,
            width: '100%',
            viewConfig: {
                loadMask: false
            },
            store: new Ext.data.Store({
                id: 'bSId',
                model: 'MadasRecentProjectsModel',
                proxy: new Ext.data.HttpProxy({
                    rootProperty: 'response.value.items',
                    versionProperty: 'response.value.version',
                    totalProperty: 'response.value.total_count',
                    url: dataurl
                })
            }),
            selModel: {
                mode : 'SINGLE'
            },
            listeners: {
                rowdblclick: function(grid, rowIndex, evt) {
                    var record = grid.store.getAt(rowIndex);
                    editProject(record.get('id'));
                }
            },
            columns: [{
                    header: 'ID',
                    sortable: true,
                    width: '20%',
                    dataIndex: 'id'
                },{
                    header: 'Title',
                    width: '40%',
                    dataIndex: 'title'
                },{
                    header: 'Client',
                    sortable: true,
                    width: '40%',
                    dataIndex: 'client'
                }
            ]}
       ]
    });
    
    return panel;
   };



MA.Dashboard.CreateRecentExperiments = function() {
    var dataurl = MA.BaseUrl + 'ws/recent_experiments';

    var editExperiment = function(experiment_id) {
        MA.ExperimentController.loadExperiment(experiment_id);
    };
    var experimentEditHandler = function(el, ev) {
        var grid = panel.items.items[0];
        var selectionModel = grid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            editExperiment(selectionModel.getSelection()[0].data.id);
        }
    };

    MA.LoadMachineAndRuleGeneratorDatastores();

    var toolbar = new Ext.Toolbar({
        items: [
            { id: 'experimentEditBtn', text: 'Edit', handler: experimentEditHandler }
        ]
    });

    var panel = new Ext.Panel({
        title: 'Recent Experiments',
        style: 'padding:20px 20px 0px 20px',
        id: 'dashboard-recent-experiments',
        tbar: toolbar,
        viewConfig: {
            loadMask: false
        },
        init: function() {
            this.items.items[0].store.load();
        },
        items: [{
            xtype: 'grid',
            border: false,
            autoScroll: true,
            stripeRows: true,
            height: 150,
            width: '100%',
            store: new Ext.data.Store({
                id: 'bSId',
                fields: [
                    { name: 'id', sortType: Ext.data.SortTypes.asInt },
                    { name: 'title', sortType: Ext.data.SortTypes.asText },
                    { name: 'status', sortType: Ext.data.SortTypes.asText }
                ],
                proxy: new Ext.data.HttpProxy({
                    reader: {
                        type: 'json',
                        rootProperty: 'response.value.items',
                        versionProperty: 'response.value.version',
                        totalProperty: 'response.value.total_count'
                    },
                    type: 'ajax',
                    url: dataurl
                })
            }),
            selMode : {
                mode : 'SINGLE'
            },
            listeners: {
                rowdblclick: function(grid, record, tr, rowIndex, evt) {
                    editExperiment(record.get('id'));
                }
            },
            columns: [{
                    header: 'ID',
                    sortable: true,
                    width: '20%',
                    dataIndex: 'id'
                },{
                    header: 'Title',
                    width: '40%',
                    dataIndex: 'title'
                },{
                    header: 'Status',
                    sortable: true,
                    width: '40%',
                    dataIndex: 'status'
                }
            ]}
       ]
    });

    return panel;
   };

/*TODO : Link this into run information*/
MA.Dashboard.CreateRecentRuns = function() {

    var dataurl = MA.BaseUrl + 'ws/recent_runs';
    var editRun = function(run_id) {
        document.location = 'repo/run/view?id=' + run_id;
    };
    var runEditHandler = function(el, ev) {
        var grid = panel.items.items[0];
        var selectionModel = grid.getSelectionModel();
        if (selectionModel.hasSelection()) {
            editRun(selectionModel.getSelection()[0].data.id);
        }
    };

    var toolbar = new Ext.Toolbar({
        items: [
            { id: 'runEditBtn', text: 'Edit', handler: runEditHandler }
        ]
    });

    var panel = new Ext.Panel({
        title: 'Recent Runs',
        style: 'padding:20px 20px 0px 20px',
        id: 'dashboard-recent-runs',
        init: function() {
            this.items.items[0].store.load();
        },
        //tbar: toolbar,
        items: [
                {
                    xtype: 'grid',
                    border: false,
                    autoScroll: true,
                    stripeRows: true,
                    height: 150,
                    loadMask : false,
                    store: new Ext.data.Store({
                        id: 'bSId',
                        fields: [
                            { name: 'id', sortType: Ext.data.SortTypes.asInt },
                            { name: 'title', sortType: Ext.data.SortTypes.asText },
                            { name: 'status', sortType: Ext.data.SortTypes.asText }
                        ],
                        proxy: {
                            type: 'ajax',
                            url: dataurl,
                            reader: {
                                type: 'json',
                                rootProperty: 'response.value.items',
                                versionProperty: 'response.value.version',
                                totalProperty: 'response.value.total_count'
                            }
                        }
                    }),
                    selModel: {
                        mode: 'SINGLE'
                    },
                    columns: [
                        {
                            header: 'ID',
                            sortable: true,
                            width: '10%',
                            dataIndex: 'id'
                        },
                        {
                            header: 'Title',
                            width: '25%',
                            dataIndex: 'title'
                        },
                        {
                            header: 'Method',
                            width: '25%',
                            dataIndex: 'method'
                        },
                        {
                            header: 'Machine',
                            width: '20%',
                            dataIndex: 'machine'
                        },
                        {
                            header: 'State',
                            sortable: true,
                            width: '20%',
                            dataIndex: 'state'
                        }
                    ]
              }
       ]
    });
    
    return panel;
   };



MA.Dashboard.CreateAvailableFiles = function() {
    var downloadClientFile = function(fileID) {
      if (fileID.toString().substr(0, 5) !== 'xnode') {
          window.location = MA.BaseUrl + 'ws/downloadClientFile/' + fileID;
      }
    };

    return new Ext.Panel({
        title: 'Available files',
        style: 'padding:20px 20px 0px 20px',
        id: 'dashboard-available-files',
        tbar: [{
            xtype: 'tbtext',
            text: 'Click a filename to download'
        }],
        init: function() {
        },
        items: [
                {
                   xtype: 'treepanel',
                   border: false,
                   autoScroll: true,
                   animate: true,
                   useArrows: true,
                   dataUrl: MA.BaseUrl + 'ws/recordsClientFiles',
                   requestMethod: 'GET',
                   rootProperty: {
                       nodeType: 'async',
                       text: 'Files',
                       draggable: false,
                       id: 'dashboardFilesRoot',
                       expanded: true,
                       metafile: true
                   },
                   selModel: new Ext.selection.TreeModel(
                       { listeners:
                           {
                               selectionchange: function(store, node) {
                                   if (node !== null && !(node[0].id === "root")) {
                                       downloadClientFile(node.id);
                                   }
                               }
                           }
                       }),
                   listeners: {
                        render: function() {
                        }
                    }
               }
       ]
    });
   };

MA.Dashboard.CreateToolbar = function() {
    return {
        init: function() {},
        style: 'padding: 20px 20px 0px 20px',
        border: false,
        init: function() {
        },
        items: [{
            xtype: 'button',
            text: 'Refresh Dashboard Data',
            handler: function() {
                    var dashboard = Ext.getCmp('dashboard-panel');
                    dashboard.init();
                }
            }]
    };
};


MA.Dashboard.CreateAdminDashboard = function() {
    return new Ext.Container({
        id: 'dashboard-panel',
        autoScroll: true,
        init: function() {
            var i;
            for (i = 0; i < this.items.length; i++) {
                this.items.items[i].init();
            }
        },
        items: [
            MA.Dashboard.CreateToolbar(),
            MA.Dashboard.CreatePendingUserRequests(),
            MA.Dashboard.CreateQuotesRequiringAttention(),
            MA.Dashboard.CreateRecentProjects(),
            MA.Dashboard.CreateRecentExperiments()
        ]
    });
};

MA.Dashboard.CreateClientDashboard = function() {
    return new Ext.Container({
        id: 'dashboard-panel',
        autoScroll: true,
        init: function() {
            var i;
            for (i = 0; i < this.items.length; i++) {
                this.items.items[i].init();
            }
        },
        items: [
            MA.Dashboard.CreateToolbar(),
            MA.Dashboard.CreateRecentExperiments(),
            MA.Dashboard.CreateAvailableFiles()
        ]
    });
};

MA.Dashboard.CreateStaffDashboard = function() {
    return new Ext.Container({
        id: 'dashboard-panel',
        autoScroll: true,
        init: function() {
            var i;
            for (i = 0; i < this.items.length; i++) {
                this.items.items[i].init();
            }
        },
        items: [
            MA.Dashboard.CreateToolbar(),
            MA.Dashboard.CreateRecentProjects(),
            MA.Dashboard.CreateRecentExperiments(),
            MA.Dashboard.CreateRecentRuns()
        ]
    });
};


MA.Dashboard.Create = function() {
    if (!MA.CurrentUser.IsLoggedIn) {
        return;
    }
    MA.Dashboard.IsCreated = true;
    if (MA.CurrentUser.IsAdmin || MA.CurrentUser.IsNodeRep) {
        return MA.Dashboard.CreateAdminDashboard();
    } else if (MA.CurrentUser.IsClient) {
        return MA.Dashboard.CreateClientDashboard();
    } else {
        return MA.Dashboard.CreateStaffDashboard();
    }
};

MA.Dashboard.IsCreated = false;
MA.Dashboard.Init = function() {
    var dboard;
    if (!MA.Dashboard.IsCreated) {
        dboard = MA.Dashboard.Create();
        if (dboard !== undefined) {
            Ext.getCmp('center-panel').add(dboard);
        }
    }
    if (MA.Dashboard.IsCreated) {
        Ext.getCmp('dashboard-panel').init();
    }
};
