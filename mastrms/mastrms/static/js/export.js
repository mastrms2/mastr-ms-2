/**
 * This file is part of Madas.
 *
 * Madas is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Madas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Madas.  If not, see <http://www.gnu.org/licenses/>.
 */

MA.ExportInit = function() {
  //Will fill with something 
}

MA.ExportCmp = {
    id: 'export-container-panel',
    layout: 'absolute',
    autoScroll: true,
    deferredRender: false,
    forceLayout: true,
    defaults: {
        deferredRender: false,
        forceLayout: true
    },
    items: [{ 
            xtype: 'form',
            labelWidth: 80, // label settings here cascade unless overridden
            id: 'export-panel',
            url: MA.BaseUrl + 'export/process',
            method: 'POST',
            frame: true,
            title: 'Export',
            bodyStyle: 'padding:10px 50px 0',
            width: 1000,
            x: 50,
            y: 10,
            defaults: {width: 400},
            defaultType: 'textfield',
            trackResetOnLoad: true,
            waitMsgTarget: true,
            items: [{ xtype: 'fieldset',
                      title: 'Date',
                      id: 'dateFieldset',
                      layout: 'hbox',
                      style: 'padding: 5px',
                      autoHeight: true,
                      labelWidth: 50,
                      width: '100%',
                      items: [
                              { xtype: 'datefield', width: '40%', fieldLabel: 'From', id: 'date_start' },
                              { xtype: 'tbspacer', width: '20%' },
                              { xtype: 'datefield', width: '40%', fieldLabel: 'To', id: 'date_end' }
                      ]
            },{ xtype: 'fieldset',
                title: 'Client Node',
                id: 'clientNodeFieldset',
                style: 'padding: 5px',
                autoHeight: true,
                labelWidth: 50,
                width: '100%',
                items: [
                        { xtype: 'textfield', width: '100%', fieldLabel: 'Site Name', id: 'site_name' },
                        { xtype: 'textfield', width: '100%', fieldLabel: 'Station Name', id: 'station_name' }
                ]
            },{ xtype: 'fieldset',
                title: 'Client',
                id: 'clientFieldset',
                style: 'padding: 5px',
                autoHeight: true,
                labelWidth: 50,
                width: '100%',
                items: [
                        { xtype: 'textfield', width: '100%', fieldLabel: 'Client Name', id: 'client_name' }
                ]
            },{ xtype: 'fieldset',
                title: 'Project Manager',
                id: 'projectManagerFieldset',
                style: 'padding: 5px',
                autoHeight: true,
                labelWidth: 50,
                width: '100%',
                items: [
                        { xtype: 'textfield', width: '100%', fieldLabel: 'Project Manager Name', id: 'projectmanager_name' }
                ]
            }],
            buttons : [ {
              text : 'Export',
              handler: function() {
                 Ext.getCmp('export-panel').getForm().submit({ 
                    successProperty: 'success',
                    success: function(form, action) {
                    if (action.result.success === true) {
                        form.reset();

                        //display a success alert that auto-closes in 5 seconds
                        Ext.Msg.alert('Export is processed', '(this message will auto-close in 5 seconds)');
                        setTimeout(Ext.Msg.hide, 5000);
                    }
                    },
                    failure: function(form, action) {
                    //do nothing special. this gets called on validation failures and server errors
                    }
                });
              }
            }]
    }]
};
