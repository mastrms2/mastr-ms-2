//MA.TrackingSm = new Ext.grid.CheckboxSelectionModel({ width: 25 });

MA.SampleTrackingInit = function() {
    var expId = MA.ExperimentController.currentId();

    var classLoader = new Ajax.Request(wsBaseUrl + 'populate_select/sampleclass/id/class_id/experiment__id/' + encodeURIComponent(expId),
                                     {
                                     asynchronous: false,
                                     evalJSON: 'force',
                                     onSuccess: function(response) {
                                         var classComboStore = Ext.StoreMgr.get('classCombo');
                                         var data = response.responseJSON.response.value.items;
                                         var massagedData = [];

                                         for (var idx in data) {
                                             massagedData[idx] = [data[idx]['key'], data[idx]['value']];
                                         }

                                         classComboStore.loadData(massagedData);
                                         }
                                     }
                                     );

    MA.SampleLoadByExperiment();
    MA.SampleLogLoad();

//    Ext.getCmp('sampleTracking').tbar.setWidth(800);
//    Ext.getCmp('sampleTracking').topToolbar.setSize(800);
};

MA.SampleLogLoad = function() {
    sampleLogStore.load({ params: { sample__experiment__id__exact: MA.ExperimentController.currentId() } });
};

MA.SampleLogSuccess = function() {
    MA.SampleLoadByExperiment();
    MA.SampleLogLoad();
};

MA.SampleTracking = {
    title: 'Sample Tracking',
    region: 'center',
    cmargins: '0 0 0 0',
    collapsible: false,
    bodyStyle: 'padding:0px;background:transparent;',
    layout: 'anchor',
    autoScroll: true,
    items: [
            {
            xtype: 'fieldset',
            bodyStyle: 'padding:15px;background:transparent;padding-bottom:0px;',
            border: false,
            autoHeight: true,
            items: [
                    { xtype: 'displayfield', fieldLabel: 'Experiment name', id: 'trackingExperimentName'},
                    { xtype: 'displayfield', fieldLabel: 'Comment', id: 'trackingExperimentComment', width: 400, height: 40 },
                    { xtype: 'displayfield', fieldLabel: 'Formal quote', id: 'trackingFormalQuote' },
                    { xtype: 'displayfield', fieldLabel: 'Organisation', id: 'trackingOrg'},
                    { xtype: 'displayfield', fieldLabel: 'Job number', id: 'trackingJobNumber' }
                ]
            },
            {
            xtype: 'grid',
            border: true,
            title: 'Samples',
            style: 'padding-left:3%;padding-right:3%;padding-bottom:10px;',
            height: 300,
            id: 'sampleTracking',
            trackMouseOver: false,
            selModel : {
              selType: 'checkboxmodel',
              injectCheckBox : 0
            },
            width: 1200,
//            collapsible:true,
//            collapsed:true,
            viewConfig: {
            forceFit: true,
            autoFill: true
            },
            tbar: {
                height: 45,
                items: [
                    { xtype: 'tbtext', text: 'Mark Selected Samples as ' },
                    { xtype: 'tbspacer', width: 15 },
                    {
                        xtype: 'combo', width: 120,
                        id: 'sampleLogType',
                        name: 'sampleLogTypeText',
                        forceSelection: true,
                        displayField: 'value',
                        valueField: 'key',
                        hiddenName: 'sampleLogTypeValue',
                        value: '1',
                        mode: 'local',
                        allowBlank: false,
                        editable: false,
                        typeAhead: false,
                        triggerAction: 'all',
                        listWidth: 230,
                        store: new Ext.data.ArrayStore({
                                                   fields: ['value', 'key'],
                                                   data: [['Received', 0],
                                                           ['Stored', 1],
                                                           ['Prepared', 2],
                                                           ['Acquired', 3],
                                                           ['Data Processed', 4]]
                                                   })
                        },
                    { xtype: 'tbspacer', width: 115 },
                    { xtype: 'textfield', id: 'sampleLogComment', width: 200 },
                    { xtype: 'tbspacer', width: 15 },
                    {
                        text: 'Save',
                        cls: 'x-btn-text-icon',
                        icon: MA.BaseUrl + 'static/images/save.png',
                        listeners: {
                            'click': function(e) {
                                var grid = Ext.getCmp('sampleTracking');
                                //save changes to selected entries
                                if (grid.getSelectionModel().getCount() > 0) {
                                    var logType = Ext.getCmp('sampleLogType').getValue();
                                    var comment = Ext.getCmp('sampleLogComment').getValue();

                                    var selections = grid.getSelectionModel().getSelection();

                                    if (!Ext.isArray(selections)) {
                                        selections = [selections];
                                    }

                                    var ids = [];
                                    for (var idx in selections) {
                                        if (!Ext.isObject(selections[idx])) {
                                            continue;
                                        }

                                        ids.push(selections[idx].data.id);
                                    }

                                    var saver = new Ajax.Request(
                                        wsBaseUrl + 'batchcreate/samplelog/?type=' + encodeURIComponent(logType) + '&description=' + encodeURIComponent(comment) + '&sample_ids=' + encodeURIComponent(ids.join(",")),
                                        {
                                            asynchronous: true,
                                            evalJSON: 'force',
                                            onSuccess: MA.SampleLogSuccess
                                        });
                                }
                            }
                        }
                    }
                ]
            },
            columns: [
                      { header: "ID", sortable: true, menuDisabled: true, dataIndex: 'id', width: '8%' },
                      { header: "Label", sortable: true, menuDisabled: true, dataIndex: 'label', width: '18%' },
                      { header: "Weight", sortable: true, menuDisabled: true, dataIndex: 'weight', width: '10%' },
                      { header: "Comment", sortable: true, menuDisabled: true, width: 300, dataIndex: 'comment', width: '24%' },
                      { header: "Class", sortable: true, menuDisabled: true, dataIndex: 'sample_class', renderer: renderClass, width: '20%' },
                      { header: "Last Status", sortable: true, menuDisabled: true, width: 300, dataIndex: 'last_status', width: '20%' }
                    ],
            store: randomisableSampleStore
            },
            {
            xtype: 'grid',
            border: true,
            title: 'Sample Log',
            style: 'padding-left:3%;padding-right:3%;',
            height: 200,
            width: 1200,
            id: 'sampleTrackingLog',
            trackMouseOver: false,
            viewConfig: {
            forceFit: true,
            autoFill: true
            },
            columns: [
                      { header: "ID", sortable: true, menuDisabled: true, dataIndex: 'id', width: '8%' },
                      { header: "Date", sortable: true, menuDisabled: true, dataIndex: 'changetimestamp', width: '16%' },
                      { header: "User", sortable: true, menuDisabled: true, dataIndex: 'user', renderer: renderUser, width: '18%' },
                      { header: "Sample", sortable: true, menuDisabled: true, dataIndex: 'sample', width: '18%' },
                      { header: "Type", sortable: true, menuDisabled: true, dataIndex: 'type', renderer: renderSampleLogType, width: '18%' },
                      { header: "Description", sortable: true, menuDisabled: true, dataIndex: 'description', width: '22%' }
                      ],
            store: sampleLogStore
            }
            ]
};
