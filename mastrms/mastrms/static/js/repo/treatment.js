MA.TreatmentInit = function() {
    MA.TimelineLoad();
    MA.TreatmentLoad();
};

MA.TimelineLoad = function() {
    timelineStore.load({ params: { experiment__id__exact: MA.ExperimentController.currentId() } });
};

MA.TreatmentLoad = function() {
    treatmentStore.load({ params: { experiment__id__exact: MA.ExperimentController.currentId() } });
};

MA.SaveTimelineRow = function(roweditor, changes) {
    var bundledData = {};
    var rec = { data: changes.newValues};

    bundledData.timeline = rec.data.timeline;
    bundledData.abbreviation = rec.data.abbreviation;

    if (!changes.record.dirty) {
        changes.record.data.id = "";
    }

    MA.SaveRowLiterals('sampletimeline', roweditor, bundledData, changes.record, MA.TimelineLoad);
};

MA.SaveTreatmentRow = function(roweditor, changes) {
    var bundledData = {};
    var rec = { data: changes.newValues};

    bundledData.name = rec.data.name;
    bundledData.type_id = rec.data.type;
    bundledData.description = rec.data.description;
    bundledData.abbreviation = rec.data.abbreviation;

    if (!changes.record.dirty) {
        changes.record.data.id = "";
    }

    MA.SaveRowLiterals('treatment', roweditor, bundledData, changes.record, MA.TreatmentLoad);
};

MA.Treatment = {
    baseCls: 'x-plain',
    border: false,
    frame: false,
    layout: 'border',
    defaults: {
        bodyStyle: 'padding:15px;background:white;'
    },
    items: [
        {
            title: 'Treatment',
            region: 'center',
            collapsible: false,
            layout: 'form',
            autoScroll: true,
            minSize: 75,
            items: [
                { xtype: 'fieldset',
                title: 'Timeline',
                autoHeight: true,
                items: [
                    { xtype: 'grid',
                            id: 'dates',
                            style: 'margin-top:10px;margin-bottom:10px;',
                            title: 'Timeline',
                            width: 700,
                            height: 400,
                            border: true,
                            trackMouseOver: false,
                            plugins: [new Ext.grid.plugin.RowEditing({saveText: 'Update', errorSummary: false, listeners: {'afteredit': MA.SaveTimelineRow}})],
                            selModel: {
                                selType: 'rowmodel',
                                mode: 'SINGLE'
                            },
                            viewConfig: {
                                forceFit: true,
                                autoFill: true
                            },
                            bbar: [
                                {
                                    text: 'Create',
                                    cls: 'x-btn-text-icon',
                                    icon: MA.BaseUrl + 'static/images/create-samples.png',
                                    handler: function() {
                                        var reps = Ext.getCmp('timelineReplicateField').getValue();

                                        var grid = Ext.getCmp('dates');

                                        for (var i = 0; i < parseInt(reps); i++) {
                                            MA.CRUDSomething('create/sampletimeline/', {
                                                'experiment_id': MA.ExperimentController.currentId(),
                                                'timeline': '00:00'
                                            }, MA.TimelineLoad);
                                        }
                                    }
                                },
                                {
                                    xtype: 'numberfield',
                                    id: 'timelineReplicateField',
                                    value: '1',
                                    width: 50
                                },
                                {
                                    xtype: 'panel',
                                    html: ' new timeline entries',
                                    border: false,
                                    bodyStyle: 'background:transparent;padding:4px; color: #333'
                                }
                            ],
                            tbar: [
                                {
                                    text: 'Add Time',
                                    cls: 'x-btn-text-icon',
                                    icon: MA.BaseUrl + 'static/images/add.png',
                                    handler: function() {
                                        MA.CRUDSomething('create/sampletimeline/', {
                                            'experiment_id': MA.ExperimentController.currentId(),
                                            'timeline': '00:00'
                                        }, MA.TimelineLoad);
                                    }
                                },
                                {
                                    text: 'Remove Time',
                                    cls: 'x-btn-text-icon',
                                    icon: MA.BaseUrl + 'static/images/delete.png',
                                    handler: function() {
                                        var grid = Ext.getCmp('dates');
                                        var delIds = [];

                                        var selections = grid.getSelectionModel().getSelection();
                                        if (!Ext.isArray(selections)) {
                                            selections = [selections];
                                        }

                                        for (var index = 0; index < selections.length; index++) {
                                            if (!Ext.isObject(selections[index])) {
                                                continue;
                                            }

                                            delIds.push(selections[index].data.id);
                                        }

                                        for (var i = 0; i < delIds.length; i++) {
                                            MA.CRUDSomething('delete/sampletimeline/' + delIds[i], {}, MA.TimelineLoad);
                                        }
                                    }
                                }
                            ],
                            columns: [
                                      { header: "Timeline", width: '50%', sortable: false, menuDisabled: true, dataIndex: 'timeline', editor: new Ext.form.TextField({allowBlank: false}) },
                                      { header: "Abbrev", width: '50%', sortable: false, menuDisabled: true, editor: new Ext.form.TextField({ allowBlank: true }), dataIndex: 'abbreviation'
                                      }
                            ],
                            store: timelineStore
                        }
                    ]
                },
                { xtype: 'fieldset',
                title: 'Treatments',
                autoHeight: true,

                items: [
                    { xtype: 'grid',
                            id: 'othertreat',
                            style: 'margin-top:10px;margin-bottom:10px;',
                            title: 'Treatments',
                            width: 700,
                            height: 400,
                            border: true,
                            trackMouseOver: false,
                            plugins: [new Ext.grid.plugin.RowEditing({saveText: 'Update', errorSummary: true, listeners: {'afteredit': MA.SaveTreatmentRow}})],
                            selModel: {
                                selType: 'rowmodel',
                                mode: 'SINGLE'
                            },
                            store: treatmentStore,
                            viewConfig: {
                                forceFit: true,
                                autoFill: true
                            },
                            tbar: [
                                {
                                    text: 'Add Treatment',
                                    cls: 'x-btn-text-icon',
                                    icon: MA.BaseUrl + 'static/images/add.png',
                                    handler: function() {
                                        MA.CRUDSomething('create/treatment/', {
                                            'experiment_id': MA.ExperimentController.currentId(),
                                            'name': 'Unknown'
                                        }, MA.TreatmentLoad);
                                    }
                                },
                                {
                                    text: 'Remove Treatment',
                                    cls: 'x-btn-text-icon',
                                    icon: MA.BaseUrl + 'static/images/delete.png',
                                    handler: function() {
                                        var grid = Ext.getCmp('othertreat');
                                        var delIds = [];

                                        var selections = grid.getSelectionModel().getSelection();
                                        if (!Ext.isArray(selections)) {
                                            selections = [selections];
                                        }

                                        for (var index = 0; index < selections.length; index++) {
                                            if (!Ext.isObject(selections[index])) {
                                                continue;
                                            }

                                            delIds.push(selections[index].data.id);
                                        }

                                        for (var i = 0; i < delIds.length; i++) {
                                            MA.CRUDSomething('delete/treatment/' + delIds[i], {}, MA.TreatmentLoad);
                                        }
                                    }
                                }
                            ],
                            columns: [
                                { header: "Name", width: '50%', sortable: false, menuDisabled: true, editor: new Ext.form.ComboBox({
                                editable: true,
                                forceSelection: false,
                                displayField: 'value',
                                lazyRender: true,
                                name: 'bullshot',
                                allowBlank: false,
                                typeAhead: false,
                                triggerAction: 'query',
                                listWidth: 230,
                                store: treatmentComboStore,
                                enableKeyEvents: true,
                                scope: this,
                                listeners: {

                                    expand: function(combo) {
                                       // Since ExtJS combobox doesn't allow selection at -1  --> we pick -2 :)
                                        this.select(-2, false);
                                    },

                                    beforequery: function() {
                                        this.collapse();
                                    },

                                    keydown: function(field, e) {
                                        if (e.getKey() == 9) {    // When pressing 'Tab'
                                            this.collapse();
                                        }
                                    }

                                }
                            }), dataIndex: 'name' },
                            { header: "Abbrev", width: '50%', sortable: false, menuDisabled: true, editor: new Ext.form.TextField({ allowBlank: true }), dataIndex: 'abbreviation'
                            }
                            ]
                      }
                    ]
                }
            ]
        }
    ]
};
