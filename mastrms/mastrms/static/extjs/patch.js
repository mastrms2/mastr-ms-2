//Ext.override(Ext.panel.Tool, {
//  iconBaseCls: 'fa cbfa-tool',
//  iconFontCls: 'fa-power-off',
//  initComponent: function () {
//    var me = this;
//
//    Ext.applyIf(me.renderData, {
//      iconBaseCls: me.iconBaseCls,
//      iconFontCls: me.iconFontCls
//    });
//    me.callParent(arguments);
//  },
//  beforeRender: function () {
//    // FIX ME: collapse is now set for only right hand side panel
//    var iconClsMap = {
//      close: 'fa-close',
//      collapse: ' fa-chevron-circle-right',
//      expand: ' fa-chevron-circle-left',
//      maximize: 'fa-arrow-circle-up fa-rotate-45',
//      restore: 'fa-arrow-circle-down fa-rotate-45',
//      unpin: 'fa-plus-circle'
//    };
//
//    var type = this.type.split('-')[0];
//    if (type in iconClsMap) {
//      this.iconFontCls = iconClsMap[type];
//      this.renderData.iconFontCls = this.iconFontCls;
//    }
//    console.log('TOOL TYPE', this.type, this.iconFontCls);
//
//    this.callParent(arguments);
//  },
//  setType: function (type) {
//    var iconClsMap = {
//      close: 'fa-close',
//      collapse: ' fa-chevron-circle-right',
//      expand: ' fa-chevron-circle-left',
//      maximize: 'fa-arrow-circle-up fa-rotate-45',
//      restore: 'fa-arrow-circle-down fa-rotate-45',
//      unpin: 'fa-plus-circle'
//    };
//
//    var me      = this,
//        oldType = this.type.split('-')[0],
//        newType = type.split('-')[0];
//
//    if (me.rendered) {
//      if (oldType && oldType in iconClsMap) {
//        me.toolEl.removeCls(iconClsMap[oldType]);
//      }
//      if (newType in iconClsMap) {
//        me.toolEl.addCls(iconClsMap[newType]);
//      }
//    }
//    return this.callParent(arguments);
//  },
//  renderTpl: [
//    '<span id="{id}-toolEl" data-ref="toolEl" class="{baseCls}-{type}{childElCls} {iconBaseCls} {iconFontCls}" role="presentation" />'
//  ]
//});