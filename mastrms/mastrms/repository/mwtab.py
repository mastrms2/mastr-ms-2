import os.path
import re
import zipfile
from numbers import Number
from collections import OrderedDict
from itertools import imap
import csv
import logging
import json
from StringIO import StringIO

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic.base import View
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist

from .models import Experiment, Project, NodeClient, MicrobialInfo, PlantInfo, AnimalInfo, HumanInfo, RunSample

logger = logging.getLogger('mastrms.mwtab')

class MwTabExportView(View) :

	def get(self, request, project_id, experiment_id):
		project = get_object_or_404(Project, id=project_id)
		experiment = get_object_or_404(Experiment, project_id=project_id, id=experiment_id)
		basename = self._archive_filename(project)

		response = HttpResponse(content_type="application/zip")
		response['Content-Disposition'] = 'attachment; filename="%s.zip"' % basename
		return self._assemble_archive(project, experiment, response, basename+"/")

	def _assemble_archive(self, project, experiment, response, prefix=""):
		archive = zipfile.ZipFile(response, "w", zipfile.ZIP_DEFLATED)
		mwtab = self.get_project_experiment_mwtab(project, experiment)
		archive.writestr(prefix + "mwtab.txt", mwtab)
		return response

	def get_project_experiment_mwtab(self, project, experiment):
		meta_fields = []
		meta_fields.extend(self.create_metabolomics_header())
		meta_fields.extend(self.create_metadata_project(project))
		meta_fields.extend(self.create_metadata_study(project, experiment))
		meta_fields.extend(self.create_metadata_subject(experiment))
		meta_fields.extend(self.create_metadata_subject_sample())
		meta_fields.extend(self.create_metadata_collection(experiment))
		meta_fields.extend(self.create_metadata_treatment())
		meta_fields.extend(self.create_metadata_sample_prep(experiment))
		meta_fields.extend(self.create_metadata_chromatography(experiment))
		meta_fields.extend(self.create_metadata_analysis(project))
		meta_fields.extend(self.create_metadata_ms(experiment))
		meta_fields.extend(self.create_metadata_ms_metabolite_data())
		meta_fields.extend(self.create_metadata_metabolites())
		meta_fields.extend(self.create_metadata_nmr())
		meta_fields.extend(self.create_metadata_nmr_binned_data())
		return self._tab_format(meta_fields)

	def create_metabolomics_header(self):
		file_name = "<filename>"
		version = ""
		created_on = ""
		metabolomics_header_meta = [
			('#METABOLOMICS WORKBENCH '+file_name,''),
			('VERSION',version),
			('CREATED_ON',created_on)
		]
		return metabolomics_header_meta

	def create_metadata_project(self, project):
		title = ''
		summary = []
		institute = ''
		department = ''
		laboratory = ''
		last_name = ''
		first_name = ''
		address = ''
		email = ''
		phone = ''

		if (project is not None):
			title = project.title
			summary_lines = self.string_justification(project.description,80)
			summary = self.convert_list_of_lines_into_lines(summary_lines)
			institute = project.client.businessCategory
			department = project.client.destinationIndicator
			laboratory = project.client.registeredAddress
			last_name = project.client.last_name
			first_name = project.client.first_name
			address = project.client.postalAddress
			email = project.client.email
			phone = project.client.telephoneNumber

		project_meta = [
				('#PROJECT',''),
				('PR:PROJECT_TITLE',title),
				('PR:PROJECT_TYPE','')]
		summary_fields = self.generate_multiple_lines_field('PR:PROJECT_SUMMARY',summary)
		project_meta.extend(summary_fields)
		project_meta.extend([
				('PR:INSTITUTE',institute),
				('PR:DEPARTMENT',department),
				('PR:LABORATORY',laboratory),
				('PR:LAST_NAME',last_name),
				('PR:FIRST_NAME',first_name),
				('PR:ADDRESS',address),
				('PR:EMAIL',email),
				('PR:PHONE',phone),
				('PR:FUNDING_SOURCE',''),
				('PR:PROJECT_COMMENTS',''),
				('PR:PUBLICATIONS','')
			])
		return project_meta

	def create_metadata_study(self, project, experiment):
		title = ''
		summary = []
		institute = ''
		department = ''
		laboratory = ''
		last_name = ''
		first_name = ''
		address = ''
		email = ''
		phone = ''
		
		if (project is not None):
			institute = project.client.businessCategory
			department = project.client.destinationIndicator
			laboratory = project.client.registeredAddress
			last_name = project.client.last_name
			first_name = project.client.first_name
			address = project.client.postalAddress
			email = project.client.email
			phone = project.client.telephoneNumber

		if (experiment is not None):
			title = experiment.title
			summary_lines = self.string_justification(experiment.description,80)
			summary = self.convert_list_of_lines_into_lines(summary_lines)
		study_meta = [
			('#STUDY',''),
			('ST:STUDY_TITLE',title),
			('ST:STUDY_TYPE','')]
		summary_fields = self.generate_multiple_lines_field('ST:STUDY_SUMMARY',summary)
		study_meta.extend(summary_fields)
		study_meta.extend([
			('ST:INSTITUTE',institute),
			('ST:DEPARTMENT',department),
			('ST:LABORATORY',laboratory),
			('ST:LAST_NAME',last_name),
			('ST:FIRST_NAME',first_name),
			('ST:ADDRESS',address),
			('ST:EMAIL',email),
			('ST:PHONE',phone),
			('ST:SUBMIT_DATE',''),
			('ST:NUM_GROUPS',''),
			('ST:TOTAL_SUBJECTS',''),
			('ST:NUM_MALES',''),
			('ST:NUM_FEMALES',''),
			('ST:STUDY_COMMENTS',''),
			('ST:PUBLICATIONS','')
		])
		return study_meta

	def create_metadata_subject(self, experiment):
		subject_type = ''
		if (experiment is not None):
			subject_type = (experiment.biologicalsource_set.all())[0].type

		subject_meta = [
			('#SUBJECT',''),
			('SU:SUBJECT_TYPE',subject_type),
			('SU:SUBJECT_SPECIES',''),
			('SU:TAXONOMY_ID',''),
			('SU:GENOTYPE_STRAIN',''),
			('SU:AGE_OR_AGE_RANGE',''),
			('SU:WEIGHT_OR_WEIGHT_RANGE',''),
			('SU:HEIGHT_OR_HEIGHT_RANGE',''),
			('SU:GENDER',''),
			('SU:HUMAN_RACE',''),
			('SU:HUMAN_ETHNICITY',''),
			('SU:HUMAN_TRIAL_TYPE',''),
			('SU:HUMAN_LIFESTYLE_FACTORS',''),
			('SU:HUMAN_MEDICATIONS',''),
			('SU:HUMAN_PRESCRIPTION_OTC',''),
			('SU:HUMAN_SMOKING_STATUS',''),
			('SU:HUMAN_ALCOHOL_DRUG_USE',''),
			('SU:HUMAN_NUTRITION',''),
			('SU:HUMAN_INCLUSION_CRITERIA',''),
			('SU:HUMAN_EXCLUSION_CRITERIA',''),
			('SU:ANIMAL_ANIMAL_SUPPLIER',''),
			('SU:ANIMAL_HOUSING',''),
			('SU:ANIMAL_LIGHT_CYCLE',''),
			('SU:ANIMAL_FEED',''),
			('SU:ANIMAL_WATER',''),
			('SU:ANIMAL_INCLUSION_CRITERIA',''),
			('SU:CELL_BIOSOURCE_OR_SUPPLIER',''),
			('SU:CELL_STRAIN_DETAILS',''),
			('SU:SUBJECT_COMMENTS',''),
			('SU:CELL_PRIMARY_IMMORTALIZED',''),
			('SU:CELL_PASSAGE_NUMBER',''),
			('SU:CELL_COUNTS',''),
			('SU:SPECIES_GROUP','')
		]
		return subject_meta

	def create_metadata_subject_sample(self):
		subject_sample_meta = [
			('#SUBJECT_SAMPLE_FACTORS:','SUBJECT(optional)[tab]SAMPLE[tab]FACTORS(NAME:VALUE pairs separated by |)[tab]Additional sample data'),
			('SUBJECT_SAMPLE_FACTORS',('','','',''))
		]
		return subject_sample_meta

	def create_metadata_collection(self, experiment):
		sample_type = ''
		if (experiment is not None):
			sample_type = (experiment.biologicalsource_set.all())[0].type

		collection_meta = [
			('#COLLECTION',''),
			('CO:COLLECTION_SUMMARY',''),
			('CO:COLLECTION_PROTOCOL_ID',''),
			('CO:COLLECTION_PROTOCOL_FILENAME',''),
			('CO:COLLECTION_PROTOCOL_COMMENTS',''),
			('CO:SAMPLE_TYPE',sample_type),
			('CO:COLLECTION_METHOD',''),
			('CO:COLLECTION_LOCATION',''),
			('CO:COLLECTION_FREQUENCY',''),
			('CO:COLLECTION_DURATION',''),
			('CO:COLLECTION_TIME',''),
			('CO:VOLUMEORAMOUNT_COLLECTED',''),
			('CO:STORAGE_CONDITIONS',''),
			('CO:COLLECTION_VIALS',''),
			('CO:STORAGE_VIALS',''),
			('CO:COLLECTION_TUBE_TEMP',''),
			('CO:ADDITIVES',''),
			('CO:BLOOD_SERUM_OR_PLASMA',''),
			('CO:TISSUE_CELL_IDENTIFICATION',''),
			('CO:TISSUE_CELL_QUANTITY_TAKEN','')
		]
		return collection_meta

	def create_metadata_treatment(self):
		treatment_meta = [
			('#TREATMENT',''),
			('TR:TREATMENT_SUMMARY',''),
			('TR:TREATMENT_PROTOCOL_ID',''),
			('TR:TREATMENT_PROTOCOL_FILENAME',''),
			('TR:TREATMENT_PROTOCOL_COMMENTS',''),
			('TR:TREATMENT',''),
			('TR:TREATMENT_COMPOUND',''),
			('TR:TREATMENT_ROUTE',''),
			('TR:TREATMENT_DOSE',''),
			('TR:TREATMENT_DOSEVOLUME',''),
			('TR:TREATMENT_DOSEDURATION',''),
			('TR:TREATMENT_VEHICLE',''),
			('TR:ANIMAL_VET_TREATMENTS',''),
			('TR:ANIMAL_ANESTHESIA',''),
			('TR:ANIMAL_ACCLIMATION_DURATION',''),
			('TR:ANIMAL_FASTING',''),
			('TR:ANIMAL_ENDP_EUTHANASIA',''),
			('TR:ANIMAL_ENDP_TISSUE_COLL_LIST',''),
			('TR:ANIMAL_ENDP_TISSUE_PROC_METHOD',''),
			('TR:ANIMAL_ENDP_CLINICAL_SIGNS',''),
			('TR:HUMAN_FASTING',''),
			('TR:HUMAN_ENDP_CLINICAL_SIGNS',''),
			('TR:CELL_STORAGE',''),
			('TR:CELL_GROWTH_CONTAINER',''),
			('TR:CELL_GROWTH_CONFIG',''),
			('TR:CELL_GROWTH_RATE',''),
			('TR:CELL_INOC_PROC',''),
			('TR:CELL_MEDIA',''),
			('TR:CELL_ENVIR_COND',''),
			('TR:CELL_HARVESTING',''),
			('TR:PLANT_GROWTH_SUPPORT',''),
			('TR:PLANT_GROWTH_LOCATION',''),
			('TR:PLANT_PLOT_DESIGN',''),
			('TR:PLANT_LIGHT_PERIOD',''),
			('TR:PLANT_HUMIDITY',''),
			('TR:PLANT_TEMP',''),
			('TR:PLANT_WATERING_REGIME',''),
			('TR:PLANT_NUTRITIONAL_REGIME',''),
			('TR:PLANT_ESTAB_DATE',''),
			('TR:PLANT_HARVEST_DATE',''),
			('TR:PLANT_GROWTH_STAGE',''),
			('TR:PLANT_METAB_QUENCH_METHOD',''),
			('TR:PLANT_HARVEST_METHOD',''),
			('TR:PLANT_STORAGE',''),
			('TR:CELL_PCT_CONFLUENCE',''),
			('TR:CELL_MEDIA_LASTCHANGED',''),
		]
		return treatment_meta

	def create_metadata_sample_prep(self, experiment):
		sops = []
		prep_notes = []
		filename = ''
		attached_pdf = ''
		if (experiment is not None):
			sops = experiment.standardoperationprocedure_set.all()
			prep_notes_lines = self.string_justification(experiment.sample_preparation_notes,80)
			prep_notes = self.convert_list_of_lines_into_lines(prep_notes_lines)
			if (len(sops) > 0):
				attached_pdf = sops[0].attached_pdf
				filename = attached_pdf.name

		sample_prep_meta = [
			('#SAMPLEPREP',''),
			('SP:SAMPLEPREP_SUMMARY',''),
			('SP:SAMPLEPREP_PROTOCOL_ID',''),
			('SP:SAMPLEPREP_PROTOCOL_FILENAME',filename)]
		sample_prep_notes_fields = self.generate_multiple_lines_field('SP:SAMPLEPREP_PROTOCOL_COMMENTS',prep_notes)
		sample_prep_meta.extend(sample_prep_notes_fields)
		sample_prep_meta.extend([
			('SP:PROCESSING_METHOD',''),
			('SP:PROCESSING_STORAGE_CONDITIONS',''),
			('SP:EXTRACTION_METHOD',''),
			('SP:EXTRACT_CONCENTRATION_DILUTION',''),
			('SP:EXTRACT_ENRICHMENT',''),
			('SP:EXTRACT_CLEANUP',''),
			('SP:EXTRACT_STORAGE',''),
			('SP:SAMPLE_RESUSPENSION',''),
			('SP:SAMPLE_DERIVATIZATION',''),
			('SP:SAMPLE_SPIKING',''),
			('SP:ORGAN',''),
			('SP:ORGAN_SPECIFICATION',''),
			('SP:CELL_TYPE',''),
			('SP:SUBCELLULAR_LOCATION','')
		])
		return sample_prep_meta

	def create_metadata_chromatography(self, experiment):
		instrument_method = None
		chromatography = None
		chromatography_summary = []
		randomization_order = []
		chromatography_comments = []
		chromatography_type = ''
		instrument_name = ''
		column_name = ''
		flow_gradient = ''
		flow_rate = ''
		column_temperature = ''
		methods_filename = ''
		solvent_a = ''
		solvent_b = ''
		methods_id = ''
		injection_temperature = ''
		internal_standard = ''
		internal_standard_mt = ''
		retention_index = ''
		retention_time = ''
		sample_injection = ''
		sampling_cone = ''
		analytical_time = ''
		capillary_voltage = ''
		oven_temperature = ''
		preconditioning = ''
		time_program = ''
		transferline_temperature = ''
		strong_wash_solvent_name = ''
		strong_wash_volume = ''
		target_sample_temperature = ''
		if (experiment is not None):
			runs = experiment.run_set.all()
			if (len(runs) > 0):
				instrument_method = runs[0].method
				if (instrument_method is not None) :
					try:
						chromatography = instrument_method.chromatography
						if (chromatography is not None) :
							chromatography_summary_lines = self.string_justification(chromatography.chromatography_summary,80)
							chromatography_summary = self.convert_list_of_lines_into_lines(chromatography_summary_lines)
							randomization_order_lines = self.string_justification(chromatography.randomization_order,80)
							randomization_order = self.convert_list_of_lines_into_lines(randomization_order_lines)
							chromatography_comments_lines = self.string_justification(chromatography.chromatography_comments,80)
							chromatography_comments = self.convert_list_of_lines_into_lines(chromatography_comments_lines)
							chromatography_type = chromatography.chromatography_type
							instrument_name = chromatography.instrument_name
							column_name = chromatography.column_name
							flow_gradient = chromatography.flow_gradient
							flow_rate = chromatography.flow_rate
							column_temperature = chromatography.column_temperature
							methods_filename = chromatography.methods_filename
							solvent_a = chromatography.solvent_a
							solvent_b = chromatography.solvent_b
							methods_id = chromatography.methods_id
							injection_temperature = chromatography.injection_temperature
							internal_standard = chromatography.internal_standard
							internal_standard_mt = chromatography.internal_standard_mt
							retention_index = chromatography.retention_index
							retention_time = chromatography.retention_time
							sample_injection = chromatography.sample_injection
							sampling_cone = chromatography.sampling_cone
							analytical_time = chromatography.analytical_time
							capillary_voltage = chromatography.capillary_voltage
							oven_temperature = chromatography.oven_temperature
							preconditioning = chromatography.preconditioning
							time_program = chromatography.time_program
							transferline_temperature = chromatography.transferline_temperature
							strong_wash_solvent_name = chromatography.strong_wash_solvent_name
							strong_wash_volume = chromatography.strong_wash_volume
							target_sample_temperature = chromatography.target_sample_temperature
					except ObjectDoesNotExist:
						logger.debug("chromatography does not exists")
		chromatography_meta = [
			('#CHROMATOGRAPHY','')]
		chromatography_summary_fields = self.generate_multiple_lines_field('CH:CHROMATOGRAPHY_SUMMARY',chromatography_summary)
		chromatography_meta.extend(chromatography_summary_fields)
		chromatography_meta.extend([
			('CH:CHROMATOGRAPHY_TYPE',chromatography_type),
			('CH:INSTRUMENT_NAME',instrument_name),
			('CH:COLUMN_NAME',column_name),
			('CH:FLOW_GRADIENT',flow_gradient),
			('CH:FLOW_RATE',flow_rate),
			('CH:COLUMN_TEMPERATURE',column_temperature),
			('CH:METHODS_FILENAME',methods_filename),
			('CH:SOLVENT_A',solvent_a),
			('CH:SOLVENT_B',solvent_b),
			('CH:METHODS_ID',methods_id),
			('CH:COLUMN_PRESSURE',''),
			('CH:INJECTION_TEMPERATURE',injection_temperature),
			('CH:INTERNAL_STANDARD',internal_standard),
			('CH:INTERNAL_STANDARD_MT',internal_standard_mt),
			('CH:RETENTION_INDEX',retention_index),
			('CH:RETENTION_TIME',retention_time),
			('CH:SAMPLE_INJECTION',sample_injection),
			('CH:SAMPLING_CONE',sampling_cone),
			('CH:ANALYTICAL_TIME',analytical_time),
			('CH:CAPILLARY_VOLTAGE',capillary_voltage),
			('CH:MIGRATION_TIME',''),
			('CH:OVEN_TEMPERATURE',oven_temperature),
			('CH:PRECONDITIONING',preconditioning),
			('CH:RUNNING_BUFFER',''),
			('CH:RUNNING_VOLTAGE',''),
			('CH:SHEATH_LIQUID',''),
			('CH:TIME_PROGRAM',time_program),
			('CH:TRANSFERLINE_TEMPERATURE',transferline_temperature),
			('CH:WASHING_BUFFER',''),
			('CH:WEAK_WASH_SOLVENT_NAME',''),
			('CH:WEAK_WASH_VOLUME',''),
			('CH:STRONG_WASH_SOLVENT_NAME',strong_wash_solvent_name),
			('CH:STRONG_WASH_VOLUME',strong_wash_volume),
			('CH:TARGET_SAMPLE_TEMPERATURE',target_sample_temperature),
			('CH:SAMPLE_LOOP_SIZE',''),
			('CH:SAMPLE_SYRINGE_SIZE','')])
		chromatography_randomizationorder_fields = self.generate_multiple_lines_field('CH:RANDOMIZATION_ORDER',randomization_order)
		chromatography_chromatographycomments_fields = self.generate_multiple_lines_field('CH:CHROMATOGRAPHY_COMMENTS',chromatography_comments)
		chromatography_meta.extend(chromatography_randomizationorder_fields)
		chromatography_meta.extend(chromatography_chromatographycomments_fields)
		return chromatography_meta

	def create_metadata_analysis(self, project):
		managers = []
		if (project is not None):
			managers = project.managers.all()
		lab_name = ''
		op_name = ''
		if (len(managers) > 0):
			manager = managers[0]
			nodes = managers[0].Nodes
			if (len(nodes) > 0) :
				node = nodes[0]
				lab_name = node
			op_name = manager.first_name+" "+manager.last_name

		analysis_meta = [
			('#ANALYSIS',''),
			('AN:ANALYSIS_TYPE', 'MS'),
			('AN:INSTRUMENT_NAME', ''),
			('AN:LABORATORY_NAME', lab_name),
			('AN:OPERATOR_NAME', op_name),
			('AN:DETECTOR_TYPE', ''),
			('AN:SOFTWARE_VERSION', ''),
			('AN:ACQUISITION_DATE', ''),
			('AN:ANALYSIS_PROTOCOL_FILE', ''),
			('AN:ACQUISITION_PARAMETERS_FILE', ''),
			('AN:PROCESSING_PARAMETERS_FILE', ''),
			('AN:DATA_FORMAT', '')
		]
		return analysis_meta

	def create_metadata_ms(self,experiment):
		instrument_method = None
		ms = None
		ms_comments = []
		instrument_type = ''
		ms_type = ''
		ion_mode = ''
		capillary_temperature = ''
		capillary_voltage = ''
		collision_energy = ''
		collision_gas = ''
		dry_gas_flow = ''
		dry_gas_temp = ''
		fragment_voltage = ''
		ion_source_temperature = ''
		ion_spray_voltage = ''
		ionization = ''
		ionization_energy = ''
		ionization_potential = ''
		mass_accuracy = ''
		precursor_type = ''
		reagent_gas = ''
		source_temperature = ''
		spray_voltage = ''
		dataformat = ''
		interface_voltage = ''
		nebulizer = ''
		octpole_voltage = ''
		resolution_setting = ''
		scan_range_moverz = ''
		scanning = ''
		scanning_cycle = ''
		scanning_range = ''
		skimmer_voltage = ''
		tube_lens_voltage = ''
		if (experiment is not None) :
			runs = experiment.run_set.all()
			if (len(runs) > 0):
				instrument_method = runs[0].method
				if (instrument_method is not None) :
					try:
						ms = instrument_method.ms
						if (ms is not None):
							ms_comments_lines = self.string_justification(ms.ms_comments,80)
							ms_comments = self.convert_list_of_lines_into_lines(ms_comments_lines)
							instrument_type = ms.instrument_type
							ms_type = ms.ms_type
							ion_mode = ms.ion_mode
							capillary_temperature = ms.capillary_temperature
							capillary_voltage = ms.capillary_voltage
							collision_energy = ms.collision_energy
							collision_gas = ms.collision_gas
							dry_gas_flow = ms.dry_gas_flow
							dry_gas_temp = ms.dry_gas_temp
							fragment_voltage = ms.fragment_voltage
							ion_source_temperature = ms.ion_source_temperature
							ion_spray_voltage = ms.ion_source_temperature
							ionization = ms.ionization
							ionization_energy = ms.ionization_energy
							ionization_potential = ms.ionization_potential
							mass_accuracy = ms.mass_accuracy
							precursor_type = ms.precursor_type
							reagent_gas = ms.reagent_gas
							source_temperature = ms.source_temperature
							spray_voltage = ms.spray_voltage
							dataformat = ms.dataformat
							interface_voltage = interface_voltage
							nebulizer = ms.nebulizer
							octpole_voltage = ms.octpole_voltage
							resolution_setting = ms.resolution_setting
							scan_range_moverz = ms.scan_range_moverz
							scanning = ms.scanning
							scanning_cycle = ms.scanning_cycle
							scanning_range = ms.scanning_range
							skimmer_voltage = ms.skimmer_voltage
							tube_lens_voltage = ms.tube_lens_voltage
					except ObjectDoesNotExist:
						logger.debug("MS does not exist")
		ms_meta = [
			('#MS',''),
			('MS:INSTRUMENT_TYPE',instrument_type),
			('MS:MS_TYPE',ms_type),
			('MS:ION_MODE',ion_mode)]
		ms_comments_fields = self.generate_multiple_lines_field('MS:MS_COMMENTS',ms_comments)
		ms_meta.extend(ms_comments_fields)
		ms_meta.extend([
			('MS:CAPILLARY_TEMPERATURE',capillary_temperature),
			('MS:CAPILLARY_VOLTAGE',capillary_voltage),
			('MS:COLLISION_ENERGY',collision_energy),
			('MS:COLLISION_GAS',collision_gas),
			('MS:DRY_GAS_FLOW',dry_gas_flow),
			('MS:DRY_GAS_TEMP',dry_gas_temp),
			('MS:FRAGMENT_VOLTAGE',fragment_voltage),
			('MS:FRAGMENTATION_METHOD',''),
			('MS:GAS_PRESSURE',''),
			('MS:HELIUM_FLOW',''),
			('MS:ION_SOURCE_TEMPERATURE',ion_source_temperature),
			('MS:ION_SPRAY_VOLTAGE',ion_spray_voltage),
			('MS:IONIZATION',ionization),
			('MS:IONIZATION_ENERGY',ionization_energy),
			('MS:IONIZATION_POTENTIAL',ionization_potential),
			('MS:MASS_ACCURACY',mass_accuracy),
			('MS:PRECURSOR_TYPE',precursor_type),
			('MS:REAGENT_GAS',reagent_gas),
			('MS:SOURCE_TEMPERATURE',source_temperature),
			('MS:SPRAY_VOLTAGE',spray_voltage),
			('MS:ACTIVATION_PARAMETER',''),
			('MS:ACTIVATION_TIME',''),
			('MS:ATOM_GUN_CURRENT',''),
			('MS:AUTOMATIC_GAIN_CONTROL',''),
			('MS:BOMBARDMENT',''),
			('MS:CDL_SIDE_OCTOPOLES_BIAS_VOLTAGE',''),
			('MS:CDL_TEMPERATURE',''),
			('MS:DATAFORMAT',dataformat),
			('MS:DESOLVATION_GAS_FLOW',''),
			('MS:DESOLVATION_TEMPERATURE',''),
			('MS:INTERFACE_VOLTAGE',interface_voltage),
			('MS:IT_SIDE_OCTOPOLES_BIAS_VOLTAGE',''),
			('MS:LASER',''),
			('MS:MATRIX',''),
			('MS:NEBULIZER',nebulizer),
			('MS:OCTPOLE_VOLTAGE',octpole_voltage),
			('MS:PROBE_TIP',''),
			('MS:RESOLUTION_SETTING',resolution_setting),
			('MS:SAMPLE_DRIPPING',''),
			('MS:SCAN_RANGE_MOVERZ',scan_range_moverz),
			('MS:SCANNING',scanning),
			('MS:SCANNING_CYCLE',scanning_cycle),
			('MS:SCANNING_RANGE',scanning_range),
			('MS:SKIMMER_VOLTAGE',skimmer_voltage),
			('MS:TUBE_LENS_VOLTAGE',tube_lens_voltage)
		])
		return ms_meta

	def create_metadata_ms_metabolite_data(self):
		#Between start and end, there will be name, sample names, and ms processed results
		ms_metabolite_data = [
			('#MS_METABOLITE_DATA',''),
			('MS_METABOLITE_DATA:UNITS',''),
			('MS_METABOLITE_DATA_START',''),
			('MS_METABOLITE_DATA_END','')
		]
		return ms_metabolite_data


	def create_metadata_metabolites(self):
		#Beween start and end, there will be name, annotation headings, and annotation data
		metabolites_meta = [
			('#METABOLITES',''),
			('METABOLITES_START',''),
			('METABOLITES_END','')
		]
		return metabolites_meta

	def create_metadata_nmr(self):
		nmr_meta = [
			('#NMR',''),
			('NM:INSTRUMENT_TYPE',''),
			('NM:NMR_EXPERIMENT_TYPE',''),
			('NM:NMR_COMMENTS',''),
			('NM:FIELD_FREQUENCY_LOCK',''),
			('NM:STANDARD_CONCENTRATION',''),
			('NM:SPECTROMETER_FREQUENCY',''),
			('NM:NMR_PROBE',''),
			('NM:NMR_SOLVENT',''),
			('NM:NMR_TUBE_SIZE',''),
			('NM:SHIMMING_METHOD',''),
			('NM:PULSE_SEQUENCE',''),
			('NM:WATER_SUPPRESSION',''),
			('NM:PULSE_WIDTH',''),
			('NM:POWER_LEVEL',''),
			('NM:RECEIVER_GAIN',''),
			('NM:OFFSET_FREQUENCY',''),
			('NM:PRESATURATION_POWER_LEVEL',''),
			('NM:CHEMICAL_SHIFT_REF_CPD',''),
			('NM:TEMPERATURE',''),
			('NM:NUMBER_OF_SCANS',''),
			('NM:DUMMY_SCANS',''),
			('NM:ACQUISITION_TIME',''),
			('NM:RELAXATION_DELAY',''),
			('NM:SPECTRAL_WIDTH',''),
			('NM:NUM_DATA_POINTS_ACQUIRED',''),
			('NM:REAL_DATA_POINTS',''),
			('NM:LINE_BROADENING',''),
			('NM:ZERO_FILLING',''),
			('NM:APODIZATION',''),
			('NM:BASELINE_CORRECTION_METHOD',''),
			('NM:CHEMICAL_SHIFT_REF_STD',''),
			('NM:BINNED_INCREMENT',''),
			('NM:BINNED_DATA_NORMALIZATION_METHOD',''),
			('NM:BINNED_DATA_PROTOCOL_FILE',''),
			('NM:BINNED_DATA_CHEMICAL_SHIFT_RANGE',''),
			('NM:BINNED_DATA_EXCLUDED_RANGE',''),
			('NM:INSTRUMENT_NAME','')
		]
		return nmr_meta;

	def create_metadata_nmr_binned_data(self):
		#Between start and end, there will be bin range(ppm), <sample names>, and <binned_data>
		nmr_binned_data = [
			('#NMR_BINNED_DATA', ''),
			('NMR_BINNED_DATA_START',''),
			('NMR_BINNED_DATA_END','')
		]
		return nmr_binned_data

	#Needs to be changed later
	@staticmethod
	def _tab_format(fields):
	    """
	    Returns an ISA-Tab format string for the list of given field names
	    and values.
	    """
	    def q(s):
	        """
	        Quotes a value string if it contains a tab or newline
	        character. Quote characters are escaped by doubling them.
	        """
	        if "\t" in s or "\n" in s:
	            return '"%s"' % s.replace('"', '""')
	        return s

	    def fmt(value):
	        if isinstance(value, list):
	            return "; ".join(map(q, value))
	        elif isinstance(value, tuple):
	            return "\t".join(map(fmt, value))
	        return q(str(value))

	    def make_entry((name, value)):
	        return "%s\t%s" % (name, fmt(value)) if value is not None else name

	    return "\n".join(map(make_entry, fields))

	@staticmethod
	def _choose_basename(title):
	    "Generates a clean filename from a title string"
	    repls = ((r"\s+", "_"), ("\n", "_"), ("'", ""), ('"', ""),
	             (r"_+", "_"), (r"^_", ""), (r"_$", ""))
	    cleaner = lambda s, (pat, repl): re.sub(pat, repl, s)
	    return reduce(cleaner, repls, title)

	def generate_multiple_lines_field (self,name, lines) :
		fields = []
		for line in lines :
			fields.append((name,line))
		if (len(fields) < 1) :
			fields.append((name,''))
		return fields

	#This is a function to make chunk of String, within the word limit
	def string_justification(self, paragraph, limit_character) :
		words = re.split(r" |\n|\t", paragraph)
		lines = []
		line = []
		total_char = 0
		cond = 0	
		for i in words:
			if (total_char == 0) :
				cond = total_char + len(i)
			else :
				cond = total_char + 1 + len(i) #Counting a space character
			if (cond <= limit_character) :
				if (total_char == 0) :
					total_char = total_char + len(i)
				else :
					total_char = total_char + 1 + len(i) #Counting a space character
				line.append(i)
			else :
				total_char = len(i)
				lines.append(line)
				line = []
				line.append(i)
		if (len(line) > 0) :
			lines.append(line)
		return lines

	def convert_list_of_lines_into_lines(self, list_lines) :
		lines = []
		for element in list_lines :
			line_str = ''
			for word in element :
				if (line_str == '') :
					line_str = line_str + word
				else :
					line_str = line_str + ' ' + word 
			lines.append(line_str)
		return lines;

	def _archive_filename(self, project):
	    return "mastrmsEXP_%s_%s" % (project.id, self._choose_basename(project.title))