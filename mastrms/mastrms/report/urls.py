# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.conf import settings

urlpatterns = patterns(
    'mastrms.report.views',
    (r'^process[/]*$', 'processReportView', {'SSL':settings.SSL_ENABLED}),
    (r'^processDownload/$', 'processReportDownload', {'SSL':settings.SSL_ENABLED})

)
