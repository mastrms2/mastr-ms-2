# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response
from django.utils import simplejson as json
from django.conf import settings
from ma_utils import webhelpers
from ma_utils.webhelpers import siteurl, wsgibase
from mastrms.app.utils.data_utils import jsonResponse, jsonErrorResponse
from mastrms.repository.models import *
from mastrms.login.URLState import getCurrentURLState
from mastrms.decorators import mastr_users_only
import md5, time
import datetime
import logging
import ast
import csv

logger = logging.getLogger('mastrms.general')

def processReportView(request, *args):
    logger.debug("***preparing Report View***")
    runs = processReport(request, args)
    return HttpResponse(json.dumps({'success': True, 'data': runs}))


@mastr_users_only
def processReportDownload(request, *args):
    logger.debug("***preparingFileDownload***")
    runs = processReport(request, args)
    response = processToFile(runs, 'csv') 
    #return HttpResponse(response, content_type='text/csv')
    logger.debug(response)
    return response


def processToFile(data, type) :
    #By default, it is csv format
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="report.csv"'

    writer = csv.writer(response, csv.excel)
    writer.writerow(['ID', 'Title', 'Node', 'Instrument' 'Experiment Title', 'Project Title', 'Client Email', 'Project Manager Email', 'Created On'])
    for r in data :
        writer.writerow([r['runs_id'], r['title'], r['site_name'], r['station_name'], r['experiment_title'], r['project_title'], r['client_email'], r['pm_email'], r['run_createdon']])

    return response

def processReport(request, *args):
    logger.debug('***processReport : enter ***' )

    result = []

    query = {}
    if (request.method == "POST") :
        params = request.POST.copy()
    else :
        params = request.GET.copy()

    query["date_start"] = params.get('date_start-inputEl', '')
    query["date_end"] = params.get('date_end-inputEl', '')
    logger.debug('Date is: %s - %s', query["date_start"], query["date_end"])

    query["site_name"] = params.get('site_name-inputEl', '')
    query["station_name"] = params.get('station_name-inputEl', '')
    logger.debug('Client Node detail %s, %s', query["site_name"], query["station_name"])

    #Passing an email
    query["client_email"] = params.get('client_name-inputEl', '')
    logger.debug('Client detail %s', query["client_email"])

    #Passing an email
    query["projectmanager_email"] = params.get('projectmanager_name-inputEl', '')
    logger.debug('Project Manager detail %s', query["projectmanager_email"])

    resultQuery = findRun(query)
    for r in resultQuery :
        #logger.debug("%s %s %s %s %s %s %s" % (r.id,r.run_title, r.nodeclient_sitename, r.experiment_title, r.project_title, r.client_email, r.pm_email))
        result.append({'runs_id' : r.id, 'title' : r.run_title, 'site_name' : r.nodeclient_sitename, 'station_name' : r.nodeclient_stationname, 'experiment_title' : r.experiment_title, 'project_id' : r.project_id, 'project_title' : r.project_title, 'client_email' : r.client_email, 'pm_email' : r.pm_email, 'run_createdon' : r.run_createdon.strftime("%d-%m-%Y")})

    logger.debug( '*** processReport : exit ***')
    #logger.debug(result)
    return result


def findRun(query) :
    #There are some runs that do not have machine
    conditions = []
    filterQueryStr = ""
    if ((query["date_start"] != None) and (query["date_start"] != '') and (query["date_start"] != 'null')) : 
        formatted_ds = datetime.datetime.strptime(query["date_start"], '%d-%m-%Y').strftime('%Y-%m-%d')
        conditions.append("R.created_on>='%s'::date" % (formatted_ds))
    if ((query["date_end"] != None) and (query["date_end"] != '') and (query["date_start"] != 'null')) :
        formatted_de = datetime.datetime.strptime(query["date_end"], '%d-%m-%Y').strftime('%Y-%m-%d')
        conditions.append("R.created_on<='%s'::date" % (formatted_de))
    if ((query["site_name"] != None) and (query["site_name"] != '') and (query["site_name"] != 'null')) :
        conditions.append("N.site_name='%s'" % query["site_name"])
    if ((query["station_name"] != None) and (query["station_name"] != '') and (query["station_name"] != 'null')) :
        conditions.append("N.station_name='%s'" % query["station_name"])
    if ((query["client_email"] != None) and (query["client_email"] != '') and (query["client_email"] != 'null')) :
        conditions.append("U.email='%s'" % query["client_email"])
    if ((query["projectmanager_email"] != None) and (query["projectmanager_email"] != '') and (query["projectmanager_email"] != 'null')) :
        conditions.append("UPM.email='%s'" % query["projectmanager_email"])
    if (len(conditions) > 0) :
        for i in xrange(0, len(conditions)) :
            filterQueryStr = filterQueryStr + " AND " + conditions[i]

    queryStr = "SELECT "
    queryStr = queryStr + "DISTINCT ON (R.id) R.id AS id, R.title AS run_title, R.created_on AS run_createdon, N.site_name AS nodeclient_sitename, N.station_name AS nodeclient_stationname, E.title AS experiment_title, P.id AS project_id, P.title AS project_title, U.email AS client_email, UPM.email AS pm_email "
    queryStr = queryStr + "FROM repository_run AS R, mdatasync_server_nodeclient AS N, repository_experiment as E, "
    queryStr = queryStr + "repository_project as P, users_user as U, repository_project_managers as PM, users_user as UPM "
    queryStr = queryStr + "WHERE R.machine_id=N.id AND R.experiment_id=E.id AND E.project_id=P.id AND P.client_id=U.id AND PM.project_id=E.project_id AND UPM.id=PM.user_id"
    queryStr = queryStr + filterQueryStr
    queryStr = queryStr + " ORDER BY R.id"
    logger.debug(filterQueryStr)
    result = Run.objects.raw(queryStr)
    return result


def convert(input):
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input
