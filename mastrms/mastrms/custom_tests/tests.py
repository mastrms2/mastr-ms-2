# Created by Jemie Effendy - eResearch Group on 16 February 2016
# This class is for downloading file testing purposes

import unittest
from mastrms.repository.views import downloadPackageProcess
from mastrms.repository.views import downloadFileProcess
from mastrms.repository.mwtab import MwTabExportView
import json, logging, os
logger = logging.getLogger(__name__)
current_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
file_1_path = current_dir+"/scratch/files/runs/2016/2/11/SMP-2-PBQC1_704-24731.D"
file_2_path = current_dir+"/scratch/files/runs/2016/2/11/SMP-3-CU1G_250_704-24732.D"
file_mw_default_path = current_dir+"/scratch/test_files/mwtab-test.txt"

#===================================================
class DownloadTestCase(unittest.TestCase):

	def test_download_zip_package(self):
		file_type="zip"
		experiment_id = "1"
		files = [(file_1_path,"none"),(file_2_path,"none")]
		full_package_name = "experiment_1_files."+file_type
		r = downloadPackageProcess(experiment_id,files,full_package_name)
		#self.assertEqual(len(r.content)/1000000,14)
		self.assertEqual(r['Content-Disposition'],'attachment; filename=experiment_1_files.'+file_type)
	
	def test_download_tar_package(self):
		file_type="tar"
		experiment_id = "1"
		files = [(file_1_path,"none"),(file_2_path,"none")]
		full_package_name = "experiment_1_files."+file_type
		r = downloadPackageProcess(experiment_id,files,full_package_name)
		#self.assertEqual(len(r.content)/1000000,55)
		self.assertEqual(r['Content-Disposition'],'attachment; filename=experiment_1_files.'+file_type)
	
	def test_download_tbz2_package(self):
		file_type="tbz2"
		experiment_id = "1"
		files = [(file_1_path,"none"),(file_2_path,"none")]
		full_package_name = "experiment_1_files."+file_type
		r = downloadPackageProcess(experiment_id,files,full_package_name)
		#self.assertEqual(len(r.content)/1000000,7)
		self.assertEqual(r['Content-Disposition'],'attachment; filename=experiment_1_files.'+file_type)
	
	def test_download_tgz_package(self):
		file_type="tgz"
		experiment_id = "1"
		files = [(file_1_path,"none"),(file_2_path,"none")]
		full_package_name = "experiment_1_files."+file_type
		r = downloadPackageProcess(experiment_id,files,full_package_name)
		#self.assertEqual(len(r.content)/1000000,14)
		self.assertEqual(r['Content-Disposition'],'attachment; filename=experiment_1_files.'+file_type)

	def test_download_zip_file(self):
		file_type="zip"
		file_name = file_1_path
		r = downloadFileProcess(file_name,file_type)
		self.assertEqual(int(r['Content-Length'])/1000000,6)
		self.assertEqual(r['Content-Disposition'],'attachment;  filename=\"SMP-2-PBQC1_704-24731.D.'+file_type+"\"")

	def test_download_tar_file(self):
		file_type="tar"
		file_name = file_1_path
		r = downloadFileProcess(file_name,file_type)
		self.assertEqual(int(r['Content-Length'])/1000000,26)
		self.assertEqual(r['Content-Disposition'],'attachment;  filename=\"SMP-2-PBQC1_704-24731.D.'+file_type+"\"")

	def test_download_tbz2_file(self):
		file_type="tbz2"
		file_name = file_1_path
		r = downloadFileProcess(file_name,file_type)
		self.assertEqual(int(r['Content-Length'])/1000000,3)
		self.assertEqual(r['Content-Disposition'],'attachment;  filename=\"SMP-2-PBQC1_704-24731.D.'+file_type+"\"")

	def test_download_tgz_file(self):
		file_type="tgz"
		file_name = file_1_path
		r = downloadFileProcess(file_name,file_type)
		self.assertEqual(int(r['Content-Length'])/1000000,6)
		self.assertEqual(r['Content-Disposition'],'attachment;  filename=\"SMP-2-PBQC1_704-24731.D.'+file_type+"\"")

	def test_export_mwtab(self):
		mwExportTabView = MwTabExportView()
		meta_fields = mwExportTabView.get_project_experiment_mwtab(None,None)
		file_fields = ''
		with open(file_mw_default_path,'r') as content_file:
			file_fields = content_file.read()
		meta_first_columns = map(lambda x: x.split('\t')[0], meta_fields.split('\n'))
		file_first_columns = map(lambda x: x.split('\t')[0], file_fields.split('\r'))
		self.assertEqual(meta_first_columns,file_first_columns)

if __name__ == '__main__':
	unittest.main()