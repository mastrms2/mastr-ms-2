from django import template

register = template.Library()

@register.simple_tag
def build():
    module = __import__("mastrms")
    return module.BUILD