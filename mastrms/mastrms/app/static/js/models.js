Ext.define('MadasResetPasswordModel', {
    extend: 'Ext.data.Model',
    fields: [
              { name: 'email', sortType: 'string' },
              { name: 'validationKey', sortType: 'string'}
    ]
});

Ext.define('MadasUserEditModel', {
    extend: 'Ext.data.Model',
    fields: [
             { name: 'email', sortType: Ext.data.SortTypes.asText },
             { name: 'firstname', sortType: Ext.data.SortTypes.asText },
             { name: 'lastname', sortType: Ext.data.SortTypes.asText },
             { name: 'telephoneNumber', sortType: Ext.data.SortTypes.asText },
             { name: 'physicalDeliveryOfficeName', sortType: Ext.data.SortTypes.asText },
             { name: 'title', sortType: Ext.data.SortTypes.asText },
             { name: 'homephone', sortType: Ext.data.SortTypes.asText },
             { name: 'isAdmin', sortType: Ext.data.SortTypes.asText },
             { name: 'isNodeRep', sortType: Ext.data.SortTypes.asText },
             { name: 'isMastrAdmin', sortType: Ext.data.SortTypes.asText },
             { name: 'isProjectLeader', sortType: Ext.data.SortTypes.asText },
             { name: 'isMastrStaff', sortType: Ext.data.SortTypes.asText },
             { name: 'node', sortType: Ext.data.SortTypes.asText },
             { name: 'status', sortType: Ext.data.SortTypes.asText },
             { name: 'dept', sortType: Ext.data.SortTypes.asText },
             { name: 'institute', sortType: Ext.data.SortTypes.asText },
             { name: 'organisation', sortType: Ext.data.SortTypes.asText },
             { name: 'address', sortType: Ext.data.SortTypes.asText },
             { name: 'supervisor', sortType: Ext.data.SortTypes.asText },
             { name: 'areaOfInterest', sortType: Ext.data.SortTypes.asText },
             { name: 'country', sortType: Ext.data.SortTypes.asText }
    ]
});

Ext.define('MadasUserModel', {
    extend: 'Ext.data.Model',
    fields: [
      { name: 'email', sortType: 'string' },
      { name: 'firstname', sortType: 'string' },
      { name: 'lastname', sortType: 'string' },
      { name: 'telephoneNumber', sortType: 'string' },
      { name: 'physicalDeliveryOfficeName', sortType: 'string' },
      { name: 'title', sortType: 'string' },
      { name: 'homephone', sortType: 'string' },
      { name: 'isAdmin', sortType: 'string' },
      { name: 'isNodeRep', sortType: 'string' },
      { name: 'node', sortType: 'string' },
      { name: 'status', sortType: 'string' },
      { name: 'dept', sortType: 'string' },
      { name: 'institute', sortType: 'string' },
      { name: 'address', sortType: 'string' },
      { name: 'supervisor', sortType: 'string' },
      { name: 'areaOfInterest', sortType: 'string' },
      { name: 'country', sortType: 'string' }
    ]
});