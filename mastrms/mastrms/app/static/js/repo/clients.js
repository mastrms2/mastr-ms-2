MA.ClientsListCmp = {
    title: 'Clients',
    region: 'center',
    cmargins: '0 0 0 0',
    collapsible: false,
    id: 'clients-list',
    bodyStyle: 'padding:0px;',
    layout: 'border',
    defaults: {
        split: true
    },
    border: false,
    items: [
        {
            xtype: 'grid',
            border: true,
            id: 'clients',
            region: 'center',
            selModel : {
                mode: 'SINGLE'
            },
            viewConfig: {
                loadMask: false,
                forceFit: true,
                autoFill: true
            },
            columns: [
                      { header: "Client?", width: '12%', sortable: true, dataIndex: 'is_client'},
                      { header: "ID", width: '10%', sortable: true, dataIndex: 'id'},
                      { header: "Name", width: '26%', dataIndex: 'name', sortable: true},
                      { header: "Email", width: '27%', dataIndex: 'email', sortable: true},
                      { header: "Organisation", width: '25%', dataIndex: 'organisationName', sortable: true}
            ],
            plugins: [new Ext.ux.grid.Search({
                disableSeparator: true,
                 mode: 'local'
                , iconCls: false
, dateFormat: 'm/d/Y'
                , minLength: 0
                , width: 150
                , position: 'top'
            })],
            store: clientsListStore,
            tbar: [
            ],
            listeners: {
                rowclick: function() {
                    var selModel = this.getSelectionModel();
                    var store = Ext.getCmp("client-project-list").getStore();

                    if (selModel.hasSelection()) {
                        var record = selModel.getSelected();
                        var clearFilter = function() {
                            this.clearFilter();
                        };

                        clientSampleStore.addListener("load", clearFilter, clientSampleStore, { single: true });
                        store.addListener("load", clearFilter, store, { single: true });

                        clientSampleStore.proxy.url = wsBaseUrl + "recordsSamplesForClient/client/" + record.data.client;
                        clientSampleStore.load();

                        store.load({ params: { client__id__exact: record.data.id } });
                    }
                    else {
                        var rejectAll = function() {
                            return false;
                        };

                        clientSampleStore.filterBy(rejectAll);
                        store.filterBy(rejectAll);
                    }
                }
            }
        },
        {
            xtype: "panel",
            region: "east",
            width: "50%",
            border: false,
            layout: "border",
            defaults: {
                split: true
            },
            items: [
                new MA.ProjectList({
                    id: "client-project-list",
                    border: true,
                    region: "north",
                    height: 200,
                    title: "Client Projects",
                    store: new Ext.data.JsonStore({
                        autoLoad: false,
                        proxy: {
                            type: 'ajax',
                            url: projectsListStore.proxy.url
                        },
                        remoteSort: false,
                        restful: true,
                        writer: new Ext.data.JsonWriter({ encode: false }),
                        sortOnLoad: true,
                        sortRoot: 'id',
                        sortInfo: {
                            field: 'id',
                            direction: 'DESC'
                        }
                    }),
                    listeners: {
                        dblclick: function(id) {
                            MA.LoadProject(id);
                        }
                    },
                    createNewProject: function() {
                        var selModel = Ext.getCmp('clients').getSelectionModel();
                        if (selModel.hasSelection()) {
                            MA.ChangeMainContent("project:new", [selModel.getSelected().get('id')]);
                        } else {
                            MA.ChangeMainContent("project:new");
                        }
                    }
                }),
                {
                    border: true,
                    title: "Client Samples",
                    xtype: 'grid',
                    border: true,
                    region: "center",
                    id: 'clients-samples',
                    trackMouseOver: false,
                    selModel : {
                        mode : 'SINGLE'
                    },
                    viewConfig : {
                        loadMask: false,
                        forceFit: true,
                        groupTextTpl: '{[ values.rs[0].data["experiment_title"] ]} &nbsp;&nbsp;({[values.rs.length]} samples)',
                        hideGroupedHeaders: true
                        },
                    columns: [
                              { header: "ID", sortable: true, dataIndex: 'id' },
                              { header: "Label", sortable: true, dataIndex: 'label' },
                              { header: "Weight", sortable: true, dataIndex: 'weight' },
                              { header: "Comment", sortable: false, sortable: true, width: 300, dataIndex: 'comment' },
                              { header: "Last Status", sortable: true, width: 300, dataIndex: 'last_status' },
                              { header: "Experiment ID", dataIndex: 'experiment_id' },
                              { header: "Experiment Title", dataIndex: 'experiment_title'},
                              { header: "Sample Class", dataIndex: 'sample_class' }
                    ],
                    store: clientSampleStore
                }
            ]
        }
    ]
};
