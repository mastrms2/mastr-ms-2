MA.SamplePrepInit = function() {
    sopStore.load({ params: { experiments__id: MA.ExperimentController.currentId() } });
};

MA.SaveSOPRow = function(roweditor, changes) {
    if (!changes.record.dirty) {
        MA.CRUDSomething('dissociate/standardoperationprocedure/experiment/' + changes.newValues.id + '/' + MA.ExperimentController.currentId(), {}, MA.Null);
    }
    MA.CRUDSomething('associate/standardoperationprocedure/experiment/' + changes.newValues.id + '/' + MA.ExperimentController.currentId(), {}, MA.SamplePrepInit);
};

MA.RemoveSOPRow = function(rec) {
    if (rec !== undefined && rec.data !== undefined && rec.data.id !== undefined && rec.data.id !== '') {
        MA.CRUDSomething('dissociate/standardoperationprocedure/experiment/' + rec.data.id + '/' + MA.ExperimentController.currentId(), {}, MA.SamplePrepInit);
    } else {
        sopStore.remove(rec);
    }
};

MA.DownloadSOPFile = function(sopID) {
    window.open(wsBaseUrl + "downloadSOPFileById/" + sopID);
};

function sopFileActionRenderer(val, metadata, rec) {
    val = rec.id;
    return '<a href="#" onclick="MA.DownloadSOPFile(\'' + val + '\')">download</a>';
}

MA.SamplePrep = {
    baseCls: 'x-plain',
    border: false,
    frame: false,
    layout: 'border',
    defaults: {
        bodyStyle: 'padding:15px;background:white;'
    },
    items: [
        {
            title: 'Sample Preparation',
            region: 'center',
            collapsible: false,
            autoScroll: true,
            layout: 'anchor',
            labelAlign: 'top',
            minSize: 75,
            items: [
                { xtype: 'grid',
                    id: 'standop',
                    style: 'margin-top:10px;margin-bottom:20px;',
                    title: 'Standard Operating Procedures Used',
                    width: 700,
                    height: 300,
                    border: true,
                    trackMouseOver: false,
                    selModel: {
                        selType: 'rowmodel',
                        mode: 'SINGLE'
                    },
                    plugins: [new Ext.grid.plugin.RowEditing({saveText: 'Update', errorSummary: false, listeners: {'edit': MA.SaveSOPRow}})],
                    viewConfig: {
                        forceFit: true,
                        autoFill: true
                    },
                    tbar: [{
                        text: 'Choose a SOP',
                        cls: 'x-btn-text-icon',
                        icon: MA.BaseUrl + 'static/images/add.png',
                        handler: function() {
                                sopStore.add(new Ext.data.Record({'id': '', 'description': ''}));
                            }
                        },
                        {
                        text: 'Remove SOP',
                        cls: 'x-btn-text-icon',
                        icon: MA.BaseUrl + 'static/images/delete.png',
                        handler: function() {
                            var grid = Ext.getCmp('standop');
                            var store = Ext.StoreMgr.get('sopStore');

                            var selections = grid.getSelectionModel().getSelection();

                            for (var index in selections) {
                                MA.RemoveSOPRow(selections[index]);
                            }
                        }
                        }
                    ],
                    columns: [
                        { header: "SOP", width: '30%', sortable: false, menuDisabled: true, editor: new Ext.form.ComboBox({
                                editable: false,
                                forceSelection: true,
                                displayField: 'value',
                                valueField: 'key',
                                lazyRender: true,
                                allowBlank: false,
                                typeAhead: false,
                                triggerAction: 'all',
                                listWidth: 230,
                                store: sopComboStore,
                                listeners: {'selectionchange': function() { }}
                            }), dataIndex: 'id', renderer: renderSOPLabel },
                        { header: "Description", width: '30%', sortable: false, menuDisabled: true, dataIndex: 'description', renderer: renderSOPDescription },
                        { header: "View", width: '40%', sortable: false, menuDisabled: false, dataIndex: 'download', renderer: sopFileActionRenderer }
                    ],
                    store: sopStore
                },{
                    id: 'samplePreparationNotes',
                    xtype: 'textarea',
                    fieldLabel: 'Notes',
                    autoScroll: true,
                    maxLength: 2000,
                    width: 500,
                    height: 200,
                    listeners: {
                        'change': function(field, newValue, oldValue) {
                            if (field.isValid()) {
                                MA.ExperimentController.updateSamplePreparationNotes(newValue);
                            }
                         }
                    }
                }


//                },
//                { xtype: 'textfield', fieldLabel: 'Weight' },
//                { xtype: 'checkbox', fieldLabel: 'Dry weight?' }
            ]
        }
    ]
};
