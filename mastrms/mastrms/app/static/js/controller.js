/**
 * This file is part of Madas.
 *
 * Madas is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Madas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Madas.  If not, see <http://www.gnu.org/licenses/>.
 */

MA.LoadMachineAndRuleGeneratorDatastores = function() {
    //These datastores are only loadable by admins, so this function
    //should only be called when initialising components that can only
    //have been created because the logged in user is an admin.
    //For instance, being able to click on certain admin menu items,
    //or the 'recent experiments' component, which only shows if you are
    //an admin.
    machineStore.load();
    enabledRuleGeneratorStore.load();

};


/**
 * madasChangeMainContent
 * acts as a shallow controller between string function names and initialization/display of pages
 * it should only be called directly when processing the return from an ajax request
 * any time you wish to change the display based on a user action you should call
 * to MA.Authorize() instead, as that will check authorization first, which can prevent odd
 * situations where a page displays but the content fails
 */
MA.ChangeMainContent = function(contentName, paramArray) {
    var showMenu = true;
    var affectMenu = true;
    var cancelBackTarget = true; //whether or not this action should be invoked if a user clicks Cancel (where the variable is obeyed)
    MA.isResetPassword = false; //This is a flag to let the app knows that resetting password is requested
    Ext.QuickTips.init();
    if (contentName.indexOf(":") > -1) {
        contentName = contentName.replace(':','_'); //In this ext version, the id naming convention is changed
    }

    switch (contentName) {

        case 'dashboard':
            //default
            MA.Dashboard.Init();
            try {
                Ext.getCmp('center-panel').layout.setActiveItem('dashboard-panel');
            } catch (e) {
                //This is has to be fixed
                console.log(e);
            }
            break;

        case 'registration':
            showMenu = false;
            cancelBackTarget = false;
            Ext.getCmp('center-panel').layout.setActiveItem('registration-container-panel');
            break;

        case 'login':
            showMenu = false;
            cancelBackTarget = false;
            MA.LoginInit(paramArray);
            Ext.getCmp('center-panel').layout.setActiveItem('login-container-panel');
            break;

        case 'login_forgotpassword':
            showMenu = false;
            cancelBackTarget = false;
            MA.ForgotPasswordInit();
            Ext.getCmp('center-panel').layout.setActiveItem('forgot-password-container-panel');
            break;

        case 'login_resetpassword':
            showMenu = false;
            cancelBackTarget = false;
            MA.isResetPassword = true;
            MA.ResetPasswordInit();
            Ext.getCmp('center-panel').layout.setActiveItem('resetpassword-container-panel');
            break;

        case 'login_processLogout':
            cancelBackTarget = false;
            MA.LogoutInit();
            break;

        case 'admin_adminrequests':
            MA.AdminRequestsInit();
            Ext.getCmp('center-panel').layout.setActiveItem('adminrequests-panel');
            break;

        case 'admin_usersearch':
            MA.UserSearchInit();
            Ext.getCmp('center-panel').layout.setActiveItem('usersearch-panel');
            break;

        case 'admin_rejectedUsersearch':
            MA.RejectedUserSearchInit();
            Ext.getCmp('center-panel').layout.setActiveItem('rejectedusersearch-panel');
            break;

        case 'admin_deletedUsersearch':
            MA.DeletedUserSearchInit();
            Ext.getCmp('center-panel').layout.setActiveItem('deletedusersearch-panel');
            break;

        case 'admin_nodelist':
            MA.NodeManagementInit();
            Ext.getCmp('center-panel').layout.setActiveItem('nodeManagementCmp');
            break;

        case 'admin_orglist':
            MA.OrgManagementInit();
            Ext.getCmp('center-panel').layout.setActiveItem('orgManagementCmp');
            break;

        case 'admin_useredit':
            cancelBackTarget = false;
            MA.AdminUserEditInit(paramArray);
            Ext.getCmp('center-panel').layout.setActiveItem('adminuseredit-container-panel');
            break;

        case 'user_myaccount':
            cancelBackTarget = false;
            MA.UserEditInit(paramArray);
            Ext.getCmp('center-panel').layout.setActiveItem('useredit-container-panel');
            break;

        case 'notauthorized':
            cancelBackTarget = false;
            Ext.getCmp('center-panel').layout.setActiveItem('notauthorized-panel');
            break;

        case 'message':
            cancelBackTarget = false;
            affectMenu = false;
            MA.Message(paramArray);
            break;

        case 'quote_request':
            MA.RequestQuoteInit();
            Ext.getCmp('center-panel').layout.setActiveItem('requestquote-container-panel');
            affectMenu = false;
            showMenu = false;
            break;

        case 'quote_list':
            MA.QuoteRequestListInit();
            Ext.getCmp('center-panel').layout.setActiveItem('quotelistpanel');
            break;

        case 'quote_edit':
            MA.QuoteRequestEditInit(paramArray);
            Ext.getCmp('center-panel').layout.setActiveItem('quoterequestedit-container-panel');
            break;

        case 'quote_listAll':
            MA.QuoteRequestListAllInit();
            Ext.getCmp('center-panel').layout.setActiveItem('quoterequestsall-panel');
            break;

        case 'quote:viewformal':
            MA.ViewFormalInit(paramArray);
            Ext.getCmp('center-panel').layout.setActiveItem('viewformalquote-container-panel');
            affectMenu = false;
            showMenu = false;
            break;

        /*case 'quote:listFormal':
            MA.FormalQuoteUserListInit();
            Ext.getCmp('center-panel').layout.setActiveItem('fquolist-panel');
            break;*/

        case 'client_list':
            clientsListStore.reload();
            Ext.getCmp('center-panel').layout.setActiveItem('clients-list');
            break;

        case 'project_list':
            clientsListStore.load();
            projectsListStore.load();

            //Clear experiment navigation
            if (Ext.getCmp('expNav')) {
                Ext.getCmp('expNav').getSelectionModel().deselectAll();
            }

            Ext.getCmp('center-panel').layout.setActiveItem('projects-list');
            break;

        case 'project_new':
            var clientId = -1;
            MA.currentProjectId = 0;
            var titlefield = Ext.getCmp('projectTitle');
            var desc = Ext.getCmp('projectDescription');
            var client = Ext.getCmp('projectClientCombo');
            var projManagers = Ext.getCmp('projManagerList');

            if (paramArray && paramArray.length === 1) {
                clientId = paramArray[0];
            }

            titlefield.setValue('');
            desc.setValue('');
            if (clientId === -1) {
                client.clearValue();
            } else {
                client.setValue(clientId);
            }
            projManagers.getStore().removeAll();

            experimentListStore.removeAll();
            Ext.getCmp('project-experiment-list').disable();
            Ext.getCmp('center-panel').layout.setActiveItem('projectCmpTitle');
            break;

        case 'project_view':
            expStatusComboStore.load();
            methodStore.load();
            MA.LoadMachineAndRuleGeneratorDatastores();
            sopLookupStore.load();
            maStaffComboStore.load();
            involvementComboStore.load();
            Ext.getCmp('center-panel').layout.setActiveItem('projectCmpTitle');
            Ext.getCmp('project-experiment-list').enable();
            break;

        case 'experiment_new':
            MA.ExperimentController.createExperiment();
            break;

        case 'experiment_clone':
            if (paramArray && paramArray.length === 1) {
                MA.AttemptCloneExperiment(paramArray[0]);
            }
            else {
                Ext.Msg.show({
                    'title': 'Clone Experiment: Error',
                    'msg' : 'No experiment ID given',
                    'buttons' : Ext.Msg.OK
                });
            }
            break;

        case 'report':
            MA.ReportInit();
            Ext.getCmp('center-panel').layout.setActiveItem('report-container-panel');
            break;

        case 'rulegenerator_list':
            ruleComponentStore.load();
            ruleGeneratorListStore.load();
            Ext.getCmp('center-panel').layout.setActiveItem('ruleGeneratorListCmp');
            break;

        case 'run_list':
            methodStore.load();
            MA.LoadMachineAndRuleGeneratorDatastores();
            runListStore.load({callback: function() {
                runListStore.sort([
                    {
                        field: 'state',
                        direction: 'DESC'
                    },
                    {
                        field: 'id',
                        direction: 'DESC'
                    }
                ]);
            }});

            Ext.getCmp('center-panel').layout.setActiveItem('runs-list');
            break;

        case 'repo_admin':
            document.location = MA.BaseUrl + 'repoadmin/';
            break;

        case 'help_screencasts-quoterequest':
            MA.ScreencastsInit('madas_requesting_quote.flv');
            Ext.getCmp('center-panel').layout.setActiveItem('screencasts-container-panel');
            break;

        case 'help_contactus':
            cancelBackTarget = false;
            affectMenu = false;
            MA.Message({'message': 'For any queries and issues please contact,<br><br>Dr.Saravanan Dayalan<br>sdayalan@unimelb.edu.au<br>+61 3 8344 2201'});
            break;

        case 'helpadmin_screencasts-forwardquoterequest':
            MA.ScreencastsInit('madas_forwarding_quoterequest.flv');
            Ext.getCmp('center-panel').layout.setActiveItem('screencasts-container-panel');
            break;

        case 'helpadmin_screencasts-forwardformal':
            MA.ScreencastsInit('madas_sending_formalquote.flv');
            Ext.getCmp('center-panel').layout.setActiveItem('screencasts-container-panel');
            break;

        case 'helpadmin_screencasts-replaceformal':
            MA.ScreencastsInit('madas_fixing_formalquote.flv');
            Ext.getCmp('center-panel').layout.setActiveItem('screencasts-container-panel');
            break;

        case 'helpadmin_screencasts-authrequest':
            MA.ScreencastsInit('madas_auth_request.flv');
            Ext.getCmp('center-panel').layout.setActiveItem('screencasts-container-panel');
            break;


        default:
            cancelBackTarget = false;
    }

    //always affect menu if we are initing the app
    if (contentName == MA.InitFunction) {
        affectMenu = true;
    }

//    if (affectMenu) {
//        if (showMenu) {
//            MA.MenuShow();
//        } else {
//            MA.MenuHide();
//        }
//    }

    MA.MenuEnsure();

    if (cancelBackTarget) {
        MA.CancelBackTarget = contentName;
    }

    //append the application path onto the URL as a means of making things bookmarkable
    //var regex = /\#.*$/;
    //window.location = window.location.replace(regex, "#" + contentName);

};

/**
 * madasInitApplication
 * initializes the main application interface and any required variables
 */
MA.InitApplication = function(appSecureUrl, email, mainContentFunction, params) {
   //various global settings for Ext
   //Ext.BLANK_IMAGE_URL = appSecureUrl + 'static/images/s.gif';
   console.log(appSecureUrl);
   Ext.QuickTips.init();
   MA.BaseUrl = appSecureUrl;

   MA.LoginSubmitURL = appSecureUrl + 'login/processLogin';
   MA.ResetUser();
   MA.InitFunction = mainContentFunction;

   // turn on validation errors beside the field globally
   Ext.form.Field.prototype.msgTarget = 'side';

   Ext.currentExperimentNavItem = 0;

   //the ViewPort defines the main layout for the entire Madas app
   //the center-panel component is the main area where content is switched in and out

   var viewport = new Ext.container.Viewport({
        layout: {
            type: 'border',
            align: 'center'
        },

        items: [
            new Ext.Component({
                region: 'north',
                el: 'north',
                height: 54
            }),
            {
                region: 'south',
                contentEl: 'south',
                height: 20
            },
            {
                region: 'center',
                id: 'center-panel',
                layout: 'card',
                activeItem: 0,
                items: [MA.LoginCmp, MA.RegistrationCmp, MA.NotAuthorizedCmp, MA.AdminUserEditCmp,
                        MA.UserEditCmp, MA.ForgotPasswordCmp, MA.ResetPasswordCmp, MA.NodeManagementCmp,
                        MA.OrgManagementCmp,
                        MA.RequestQuoteCmp, MA.QuoteRequestEditCmp, /*MA.ViewFormalCmp,
                        */MA.ExperimentCmp,
                        MA.ProjectListCmp, MA.ProjectCmp,
                        MA.ClientsListCmp,
                        MA.RunListCmp,
                        MA.RuleGeneratorListCmp,
                        MA.ScreencastsCmp, MA.ReportCmp]
            }
            ]
    });

    MA.MenuRender(email);

    var paramArray;
    if (params) {
        paramArray = JSON.parse(params); //eval is evil
    }

    /*MA.ExperimentController.mask = new Ext.LoadMask('center-panel', {
        removeMask: true
    });*/

    MA.GetUserInfo(function() {
        MA.ChangeMainContent(mainContentFunction, paramArray);
    });
};


MA.Message = function(paramArray) {

    Ext.Msg.alert('', paramArray.message);

};
