#!/bin/sh
#
# Script to control Mastrms in dev and test
#

# break on error
set -e -x

ACTION=$1

PORT='8000'

PROJECT_NAME='mastrms'
VIRTUALENV="${WORKSPACE}/env"
CUSTOMTEST=${WORKSPACE}/mastrms/mastrms/custom_tests

DJANGO_SETTINGS_MODULE=mastrms.settings
DBUSER=mastrmsapp



activate_virtualenv() {
    . ${VIRTUALENV}/bin/activate
}

deactivate_virtualenv() {
    deactivate
}


defaults() {
    : ${DBSERVER:="db"}
    : ${DBPORT:="5432"}
    : ${WEBSERVER="web"}
    : ${WEBPORT="8000"}
    : ${CACHESERVER="cache"}
    : ${CACHEPORT="11211"}

    : ${DBUSER="webapp"}
    : ${DBNAME="${DBUSER}"}
    : ${DBPASS="${DBUSER}"}
    export DBSERVER DBPORT DBUSER DBNAME DBPASS
}


django_defaults() {
    : ${DEPLOYMENT="dev"}
    : ${PRODUCTION=0}
    : ${DEBUG=1}
    : ${MEMCACHE="${CACHESERVER}:${CACHEPORT}"}
    : ${WRITABLE_DIRECTORY="/data/scratch"}
    : ${STATIC_ROOT="/data/static"}
    : ${MEDIA_ROOT="/data/static/media"}
    : ${LOG_DIRECTORY="/data/log"}
    : ${DJANGO_SETTINGS_MODULE="django.settings"}

    echo "DEPLOYMENT is ${DEPLOYMENT}"
    echo "PRODUCTION is ${PRODUCTION}"
    echo "DEBUG is ${DEBUG}"
    echo "MEMCACHE is ${MEMCACHE}"
    echo "WRITABLE_DIRECTORY is ${WRITABLE_DIRECTORY}"
    echo "STATIC_ROOT is ${STATIC_ROOT}"
    echo "MEDIA_ROOT is ${MEDIA_ROOT}"
    echo "LOG_DIRECTORY is ${LOG_DIRECTORY}"
    echo "DJANGO_SETTINGS_MODULE is ${DJANGO_SETTINGS_MODULE}"

    export DEPLOYMENT PRODUCTION DEBUG DBSERVER MEMCACHE WRITABLE_DIRECTORY STATIC_ROOT MEDIA_ROOT LOG_DIRECTORY DJANGO_SETTINGS_MODULE
}

activate_virtualenv

defaults
django_defaults


TEST_CASES="${WORKSPACE}/mastrms/mastrms"
django-admin.py test ${TEST_CASES} 2>&1 | tee /data/runtests.log


# Dactivate the virutalenv
deactivate_virtualenv


