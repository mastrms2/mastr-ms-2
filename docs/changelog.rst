.. _changelog:

Release Notes
=============

This page lists what changed in each Mastr-MS 2 version. For
instructions on how to upgrade the server, see
:ref:`server-upgrade`. For instructions on how to upgrade the datasync
client, see :ref:`client-upgrade`.


.. _0.1.0:

0.1.0 (1st February 2017)
-------------------------

This is the first release of MASTR-MS 2

 * Downloading multiple packages/files
 * File format download consistency
 * Call stack log
 * Major upgrade for Front-End plugin
 * Fix encoding problem writing to Database
 * Adding Export to MW-TAB feature
 * Adding component (Models and UI) of Chromatography and MS in Instrument Method Repository Admin
 * Reporting
