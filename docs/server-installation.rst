MA Mastr-MS 2 Server
===============

The MA Mastr-MS 2 server is a Django application which provides a web
interface for user management, sample management, and access to
experiment data.

MA Mastr-MS 2 is distributed as a RPM for Redhat systems 


Installation on CentOS
----------------------

..  _yum-repos:

Yum repository setup
~~~~~~~~~~~~~~~~~~~~

To satisfy dependencies, `Epel`_ and `IUS`_ repos need to be enabled.

For Centos 6.x (x86_64) user::

    sudo rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    sudo rpm -Uvh http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-11.ius.centos6.noarch.rpm

For Centos 7.x (x86_64) user::

    sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    sudo rpm -Uvh https://centos7.iuscommunity.org/ius-release.rpm 

.. _Epel: http://fedoraproject.org/wiki/EPEL
.. _IUS: http://iuscommunity.org

Next, download the ma-releases.noarch.rpm and the eresearch.crt from "release rpm" folder from https://bitbucket.org/mastrms2/mastr-ms-2-aux/src/

Then, to avoid the conflict of packages, you run::

    sudo yum install vim ca-certificates openssl httpd glibc.i686 mod_ssl

We store the rpms in our secure repository. Hence, an extra step to validate peer certificate can be done as below::

    run sudo update-ca-trust enable
    move the eresearch.crt into /etc/pki/ca-trust/source/anchors/ folder
    run sudo update-ca-trust extract


Finally, run these commands to add a search index rpm packages into our repository server::
    
    sudo rpm -Uvh path-to-file/ma-releases-1.0-2.noarch.rpm

Dependencies
~~~~~~~~~~~~

The database server package isn't a dependency of the Mastr-MS 2 RPM, so
it has to be installed manually::

    sudo yum install postgresql-server

.. note:: At present only PostgreSQL is supported.


Install
~~~~~~~

Install the Mastr-MS 2 RPM, replacing ``X.X.X`` with the desired version::

    sudo yum install mastrms2-X.X.X


Database Setup
~~~~~~~~~~~~~~

If starting from a fresh CentOS install, you will need to configure
PostgreSQL::

    service postgresql initdb
    service postgresql start
    chkconfig postgresql on

To enable password authentication in PostgreSQL, you need to edit
``/var/lib/pgsql/data/pg_hba.conf``. As described in `the
documentation`_, add the following line to ``pg_hba.conf``::

    # TYPE  DATABASE    USER        CIDR-ADDRESS          METHOD
    host    all         all         127.0.0.1/32          md5

Then restart postgresql.

.. _the documentation:
   http://www.postgresql.org/docs/8.4/static/auth-pg-hba-conf.html


Database Creation
~~~~~~~~~~~~~~~~~

Create the database in the normal way for Django/PostgreSQL (default username and password are mastrms)::

    sudo su postgres
    createuser -e -DRS -P mastrms
    createdb -e -O mastrms mastrms
    exit

The default database, username, password are all set to
*mastrms*. These settings can be changed, see (:ref:`django-settings`).

Database Population
~~~~~~~~~~~~~~~~~~~

Run Django syncdb and South migrate::

    sudo mastrms syncdb
    sudo mastrms migrate

Django will prompt to create a superuser. If you choose to create a
superuser, ensure the username and e-mail address are exactly the
same, otherwise you won't be able to log in.

Alternatively, you can use the preconfigured user
``admin@example.com`` with password ``admin`` to log in. Once you have
set up your own users, the ``admin@example.com`` user can be deleted.


Apache Web Server
~~~~~~~~~~~~~~~~~

The Mastr-MS 2 RPM installs an example Apache config file. For a user with
apache/httpd version below 2.4, can see the example at 
``/etc/httpd/conf.d/mastrms.ma``. Otherwise, it will be in 
``/etc/httpd/conf.d/mastrms-apache24.ma``. This config(s) is designed to 
work out of the box with an otherwise unconfigured CentOS Apache
installation. All that is needed is to rename ``mastrms.ma`` or 
``mastrms-apache24.ma`` to ``mastrms.conf`` so that Apache will pick it up.

If you have already made changes to the default Apache configuration,
you may need to tweak ``mastrms.conf`` so that the existing setup is
not broken. It may be necessary to consult the `Apache`_ and
`mod_wsgi`_ documentation for this.

.. _Apache: http://httpd.apache.org/docs/2.2/
.. _mod_wsgi: http://code.google.com/p/modwsgi/wiki/ConfigurationGuidelines

..  _sync-user:

To improve the performance of the server, you can download ``mod_deflate.conf`` file,
and copy that into ``/etc/httpd/conf.d/`` folder. Then, restart the httpd service to
take the changes effect. This will allow the gzip compression page load. 

User Creation
~~~~~~~~~~~~~

It is a good idea to create a special user and group for data
syncing::

    SYNCUSER=maupload
    adduser $SYNCUSER
    su $SYNCUSER -c "mkdir --mode=700 /home/$SYNCUSER/.ssh"

..  _sync-repo:

Data Repository and Permissions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default, the data repository is located at
``/var/lib/mastrms/scratch``.

There should be ample disk space on this filesystem and some data
redundancy would be desirable. If this is not the case, then you could
mount a suitable file system at this path. If the data repository
needs to be at another location, its path can be configured in the
settings file.

The data sync user needs to be able to write to this directory, and
the web server user needs to be able to read from this directory, so::

    DATADIR=/var/lib/mastrms/scratch
    mkdir -p $DATADIR/files $DATADIR/quotes
    chown maupload:maupload $DATADIR $DATADIR/*
    chmod 2770 $DATADIR $DATADIR/*

Also add the web server user to the ``maupload`` group so that it can
read/write the data which ``maupload`` has rsynced in::

    usermod -a -G maupload apache

.. _django-settings:

Django Settings File
~~~~~~~~~~~~~~~~~~~~

The config file for Mastr-MS 2 is installed at
``/etc/mastrms/mastrms.conf``. It contains basic settings_ that need
to be changed for most sites, for example the database password.

SELinux and Mastr-MS 2
~~~~~~~~~~~~~~~~~~~~~~

Mastr-MS 2 does not yet ship with a SELinux policy.
For Mastr-MS 2 to function correctly on a CentOS server, SELinux must be
disabled.

The CentOS wiki contains `instructions`_ on how to disable SELinux.

.. _instructions:
   http://wiki.centos.org/HowTos/SELinux#head-430e52f7f8a7b41ad5fc42a2f95d3e495d13d348


.. _server-upgrade:

Upgrading to a new version
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. warning:: Some versions will require "database migrations" to
   update the database. While every care is taken to ensure smooth
   upgrades, we still advise to make a backup of the database before
   upgrading. This can be done with a command such as::

       su - postgres -c "pg_dump mastrms | gzip > /tmp/mastrms-$(date +%Y%m%d).sql.gz"

Before upgrading, please check the :ref:`changelog` for any
special information relating the new version.

Install the Mastr-MS 2 RPM, replacing ``X.X.X`` with the desired version::

    sudo yum install mastrms2-X.X.X

Run Django syncdb and South migrate::

    sudo mastrms syncdb
    sudo mastrms migrate

Testing
~~~~~~~

.. warning:: Since Mastr-MS 2 runs over https, mod_ssl is needed. 
   Please make sure you have installed it before running the httpd server.
   If you have a problem with WSGI, you may need to check whether you have 
   installed mod_wsgi. This is most likely to be a problem of Centos 7 User

After changing the configuration or upgrading, start/restart the web
server with::

    service httpd restart

Mastr-MS 2 is available at https://your-web-host/mastrms/. A login page
should be visible at this URL.

Mastr-MS 2 Settings File
------------------------

The Mastr-MS 2 settings file is called ``mastr-ms.conf`` or
``mastrms.conf`` depending on the system.

The following settings are customizable. There are also comments and
example values for each setting within the settings file.

+---------------------------+-----------------------------------------------------+
| Option                    | Description                                         |
+===========================+=====================================================+
| ``dbtype``                | Database backend -- always use ``pgsql``.           |
+---------------------------+-----------------------------------------------------+
| ``dbname``                | The rest are standard database connection           |
+---------------------------+ options.                                            |
| ``dbuser``                |                                                     |
+---------------------------+                                                     |
| ``dbpass``                |                                                     |
+---------------------------+-----------------------------------------------------+
| ``dbserver``              | Optional settings for remote database               |
+---------------------------+ connection.                                         |
| ``dbport``                |                                                     |
+---------------------------+-----------------------------------------------------+
| ``memcache``              | Optional connection string(s) for ``memcached``.    |
|                           | Multiple servers are separated by spaces.           |
+---------------------------+-----------------------------------------------------+
| ``allowed_hosts``         | Space-separated list of address permitted to access |
|                           | the server.  Wildcards can be used as in the        |
|                           | `ALLOWED_HOSTS`_ docs.                              |
+---------------------------+-----------------------------------------------------+
| ``server_email``          | "From" e-mail address for server-generated error    |
|                           | mails.                                              |
+---------------------------+-----------------------------------------------------+
| ``email_host``            | Details for SMTP server. User and password are      |
+---------------------------+ optional.                                           |
| ``email_port``            |                                                     |
+---------------------------+                                                     |
| ``email_host_user``       |                                                     |
+---------------------------+                                                     |
| ``email_host_password``   |                                                     |
+---------------------------+-----------------------------------------------------+
| ``alert_email``           | Where error messages are sent.                      |
+---------------------------+-----------------------------------------------------+
| ``return_email``          | The "From" address on e-mail sent by mastr-ms 2.    |
+---------------------------+-----------------------------------------------------+
| ``logs_to_email``         | E-mail address to receive datasync client log       |
|                           | notifications.                                      |
+---------------------------+-----------------------------------------------------+
| ``keys_to_email``         | E-mail address to receive datasync key upload       |
|                           | notifications.                                      |
+---------------------------+-----------------------------------------------------+
| ``registration_to_email`` | E-mail address to receive registration              |
|                           | requests.                                           |
+---------------------------+-----------------------------------------------------+
| ``repo_user``             | Mastr-MS 2 will attempt to change ownership of data |
+---------------------------+ files to this user and group.                       |
| ``repo_group``            |                                                     |
+---------------------------+-----------------------------------------------------+
| ``repo_files_root``       | Location of data files for experiments and quotes.  |
+---------------------------+                                                     |
| ``quote_files_root``      |                                                     |
+---------------------------+-----------------------------------------------------+
| ``secret_key``            | Needs to be a secret random string, can be          |
|                           | generated by a `key generator program`_.            |
+---------------------------+-----------------------------------------------------+
| ``debug_print_stack``     | Settings to enabled/disabled logging call stack     |
|                           | information (debugging purposes)                    |
+---------------------------+-----------------------------------------------------+

.. _`ALLOWED_HOSTS`: https://docs.djangoproject.com/en/1.5/releases/1.5/#allowed-hosts-required-in-production
.. _`key generator program`: http://www.miniwebtool.com/django-secret-key-generator/

More advanced options appear in ``settings.py``. Any of the `Django
Settings`_ can be changed in this file.

.. _`Django Settings`: https://docs.djangoproject.com/en/1.6/ref/settings/


.. _administration:

Administration
--------------

There are two levels of administration necessary for Mastr-MS 2.

 * **Management**

   This involves administrating users, projects, quotes, experiments,
   etc. The URL for management is the normal Mastr-MS 2 address, but
   only users who are in the admin group can see the interface.

   https://your-web-host/mastrms/

   The management interface is described in :ref:`usage`.

 * **Django Admin**

   This involves manipulation of database objects to configure the
   data sync system. Only admin users can access the address:

   https://your-web-host/mastrms/repoadmin/

   The Django Admin site can also be accessed from *Dashboard →
   Repository → Admin*.

.. _nodeclient-setup:

Data Sync Node Client Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Configuration of a new site is done by adding a *Node client* using
the **Django Admin**. The fields should be set as follows.

+--------------------+------------------------------------------------+
| Field              | Description                                    |
+====================+================================================+
| Organisation name  | These values determine how the node is visible |
+--------------------+ in the data sync client.                       |
| Site name          |                                                |
+--------------------+                                                |
| Station name       |                                                |
+--------------------+------------------------------------------------+
| Default data path  | This should be a subdirectory of ``$DATADIR``  |
|                    | (see :ref:`sync-repo`).                        |
+--------------------+------------------------------------------------+
| Username           | This should be the data sync user              |
|                    | (see :ref:`sync-user`).                        |
+--------------------+------------------------------------------------+
| Hostname           | The hostname or IP address of the Mastr-MS 2   |
|                    | server.                                        |
+--------------------+------------------------------------------------+
| Flags              | This controls the options the data sync client |
|                    | will pass to rsync. They should always be set  |
|                    | to ``--protocol=30 -rzv --chmod=ug=rwX``.      |
+--------------------+------------------------------------------------+


Instrument Method Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before runs can be created, an *Instrument method* must be created
using the **Django Admin**. At present, the Instrument Method object
isn't used, but it must be set. The fields should be set as follows.

+--------------------+------------------------------------------------+
| Field              | Description                                    |
+====================+================================================+
| Title              | Default Method                                 |
+--------------------+------------------------------------------------+
| Method path        | A folder path on the lab machine, e.g.         |
|                    | ``D:\mastrms``                                 |
+--------------------+------------------------------------------------+
| Method name        | Default Method                                 |
+--------------------+------------------------------------------------+
| Version            | 1                                              |
+--------------------+------------------------------------------------+
| Creator            | *Your own username*                            |
+--------------------+------------------------------------------------+
| Template           | CSV                                            |
+--------------------+------------------------------------------------+
| The other fields   | *Blank*                                        |
+--------------------+------------------------------------------------+

Standard Operating Procedure Documents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you would like to make SOP documents available for viewing, you can
create objects in the Django Admin within the Repository / Standard
operation procedures page.

Once the documents are uploaded, they can be attached to experiments
and viewed through the Experiment Sample Preparation screen.

.. _adding-keys:

SSH Key Management
~~~~~~~~~~~~~~~~~~

When the data sync clients hit *Send Key*, it sends the client's
public key via a HTTP post to a URL at the Mastr-MS 2 site, and a view
handles this, saving it to the ``publickeys`` directory on the
server. It then sends an e-mail to the admins configured for the site,
telling them that a new key has been uploaded, and they should append
it on to the ``authorized_keys`` for the data sync user.

To install the key, run::

     cat $DATADIR/files/publickeys/$ORG.$SITE.$STATION_id_rsa.pub \
         >> /home/$SYNCUSER/.ssh/authorized_keys

(Replace ``$DATADIR``, ``$SYNCUSER`` and ``$ORG.$SITE.$STATION`` with
your actual settings and the information from the e-mail.)

Once the key is added, the client should be able to "Handshake" with
the server (see :ref:`client-config`).

If the key isn't working, try checking the `authorized_keys
permissions`_.

.. _authorized_keys permissions:
   http://www.openssh.org/faq.html#3.14
