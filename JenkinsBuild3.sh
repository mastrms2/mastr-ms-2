# break on error
set -e -x



CCGSOURCEDIR=${WORKSPACE}
export CCGSOURCEDIR

#export FPM_EDITOR="cat $CCGSOURCEDIR/centos/mastrms.spec"
#fpm -e -s dir -t rpm -C $CCGSOURCEDIR --name mastrms --version 1.13.$BUILD_NUMBER mastrms

echo '%_topdir %(echo ${CCGSOURCEDIR})/rpmbuild' > ~/.rpmmacros

cd $CCGSOURCEDIR && rpmbuild -bb centos/mastrms.spec