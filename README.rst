Mastr-MS 2
==========

Mastr-MS 2 is a web-based tool for experimental design, sample metadata configuration, and sample data acquisition. This is a next step project from Mastr-MS and developed by Metabolomics Australia (https://www.metabolomics.com.au/).

Documentation
-------------

Full documentation is available at https://mastr-ms-2.readthedocs.org/
or within the ``docs`` subdirectory of this source distribution.

Licence
-------

GNU GPL v3. Please contact the Metabolomics Australia if you
require a licence other than GPL for legal or commercial reasons.

For developers
--------------

We do our development using Docker containers. See: https://www.docker.com/.
You will have to set up Docker on your development machine.

Other development dependencies are Python 2 and virtualenv (https://virtualenv.pypa.io/en/latest/).

All the development tasks can be done by using the ``develop.sh`` shell script in this directory.
Please run it without any arguments for help on its usage.

Some typical usages are:

- ./develop.sh start
        To start up all the docker containers needed for dev. 
        You can access the Mastrms application on http://localhost:8000
        (replace localhost with ``$ boot2docker ip`` if using boot2docker) after this.
        You can login with one of the default users *demo/demo* or *admin/admin*.

- ./develop.sh runtests
        Starts up all the docker containers and runs all our tests against them.

- ./develop.sh pythonlint
        Lint your python code.

- ./develop.sh jslint
        Lint your javascript code.

Note: Our docker containers are coordinated using docker-compose (https://docs.docker.com/compose/) but docker-compose will be installed into a virtualenv environment automatically by the ``./develop.sh`` script for you.

Contributing
------------

1. Fork next_release branch
2. Make changes on a feature branch
3. Submit pull request

Latest Releases
---------------

0.1.0_  (1st February 2017)
  Initial development.
  Base Layout changes.
  Export to MW-TAB and ISA-TAB.
  Additional Feature Report.

.. _0.1.0: http://mastr-ms-2.readthedocs.io/en/latest/changelog.html#st-february-2017